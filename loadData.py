

def loadCorreTxt(pathfileload):

    f = open(pathfileload,"r")
    matriz = []
    readed =  False
    datap = f.readline()
    
    while(readed == False):
        data = f.readline()
        fil = data.split(" ")
        fil  = fil[0:len(fil)-1]
        filnew = [] 
        for d in fil:
            if(d!=''):
                #print(d)
                filnew.extend([float(d)])
        if(filnew==[]):
            readed = True
        if(filnew!=[]):
            matriz.extend([filnew])

    return matriz


import numpy as np
import matplotlib.pyplot as plt
import math



pathfileload  = "D:/alejandro/ProyectofGradute/mainProyect/impedance-project/pruevas/aguaLimon/agua_0.txt"

M1 = loadCorreTxt(pathfileload)

[f,fase,amp,ampr] = np.transpose(M1)

sp = 1

spv = np.ones(len(f))*sp

#plt.figure()
#plt.grid()
#plt.semilogx(f, amp)    # Bode magnitude plot
#plt.figure()
#plt.semilogx(f, fase)  # Bode phase plot
#plt.show()


fig, ax = plt.subplots()  # Create a figure and an axes.


ax.plot(f,fase,'y',label='200mm^3')
ax.set_xscale("log", nonposx='clip')
ax.set_ylim(bottom=0.1)
plt.grid()
#ax.plot(f,XI[2],'g',label='400mm^3')
#ax.plot(f,XI[3],'m',label='600mm^3')
#ax.plot(f,XI[4],'r',label='800mm^3')

ax.set_xlabel('frecuencia [Hz]')  # Add an x-label to the axes.
ax.set_ylabel('fase [ ° ]')  # Add a y-label to the axes.
ax.set_title("Jugo de limon disuelto en 100ml de agua ")  # Add a title to the axes.

ax.legend()  # Add a legend.
plt.show()


