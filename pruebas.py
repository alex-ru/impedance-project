import matplotlib.pyplot as plt
import numpy as np
import xlrd
import math 

def getErr(signal,ST):
    err = []
    for i in range(len(ST)):
        err.extend([((signal[i] - ST[i])/ST[i])*100])
    return err
def setOffset(offset,v):
    for i in range(len(v)):
        v[i] = v[i] + offset
    return v
def get_r2_python(x_list, y_list):
       n = len(x_list)
       x_bar = sum(x_list)/n
       y_bar = sum(y_list)/n
       x_std = math.sqrt(sum([(xi-x_bar)**2 for xi in x_list])/(n-1))
       y_std = math.sqrt(sum([(yi-y_bar)**2 for yi in y_list])/(n-1))
       zx = [(xi-x_bar)/x_std for xi in x_list]
       zy = [(yi-y_bar)/y_std for yi in y_list]
       r = sum(zxi*zyi for zxi, zyi in zip(zx, zy))/(n-1)
       return r**2

loc = ('control_1.xlsx') 
  
# To open Workbook 
wb = xlrd.open_workbook(loc) 
sheet = wb.sheet_by_index(0) 

print(sheet.nrows)
ST  = []
amp = []
f   = []
 
for i in range(sheet.nrows):
     f1 = sheet.cell_value(i,0)
     a = sheet.cell_value(i,3)
     s = sheet.cell_value(i,4)
     ST.extend([s])
     amp.extend([a])
     f.extend([f1])

   

fig, ax = plt.subplots()  # Create a figure and an axes.
inset_ax = fig.add_axes([.25, .62, .4, .2], facecolor='w')
offset = 0.0745
amp = setOffset(offset,amp)

ST[101] = 1.00001
ni = 5
nf = len(ST)

f   =f[ni:nf]
ST  = ST[ni:nf]
amp = amp[ni:nf]

err = getErr(amp,ST)
r2 = get_r2_python(amp,ST)
#r2 = 0



ax.plot(f,ST,label='amplitud deseada ')
ax.plot(f,amp,label=' amplitud medida ')
inset_ax.plot(f,err,'b',label=' err ')
inset_ax.grid()
inset_ax.set_title(" residual (%)")
#ax.plot(f,err,label=' err ')
ax.set_xlabel('frecuencia [Hz]')  # Add an x-label to the axes.
ax.set_ylabel('amplitud [ v ]')  # Add a y-label to the axes.
ax.set_title('Control de amplitud de señal de estimulacion' )  # Add a title to the axes.
ax.set_ylim([0.4, 1.5])
#ax.set_title('cafe sin pulpa fermentando (20 litros) {}'.format(t0.date()))  # Add a title to the axes.
ax.grid()
ax.legend()  # Add a legend.
plt.show()

