#include <OneWire.h>                
#include <DallasTemperature.h>
OneWire ourWire(2);                //Se establece el pin 2  como bus OneWire
DallasTemperature sensors(&ourWire); //Se declara una variable u objeto para nuestro sensor

// DTH11
#include "DHT.h"
#define DHTPIN 9     // what pin we're connected to
#define DHTTYPE DHT11   // DHT 11 
DHT dht(DHTPIN, DHTTYPE);

void setup() {
delay(1000);
Serial.begin(9600);
sensors.begin();   //Se inicia el sensor
dht.begin();
}
 
void loop() 
{
sensors.requestTemperatures();   //Se envía el comando para leer la temperatura
float tempCafe = sensors.getTempCByIndex(0); //Se obtiene la temperatura en ºC

float humedad = dht.readHumidity();
float tempAmbiente = dht.readTemperature();

float  ph = getpH();

//Serial.println(" temp cafe :" + String(tempCafe));
//Serial.println(" temp ambiente :" + String(tempAmbiente));
//Serial.println(" humedad :" + String(humedad));
//Serial.println(" pH :" + String(ph));
//Serial.println("");
String datos = String(tempCafe)+"," +String(tempAmbiente)+","+ String(humedad)+"," + String(ph) +"a" ;
Serial.println(datos);
//Serial.println(String(tempCafe)+"a");
//Serial.println(String(tempAmbiente)+"b");
//Serial.println(String(humedad)+"c");
//Serial.println(String(ph)+ "d");

delay(1000);                     
}

float getpH(){
  
  float ph;
  float phM=0;
  int n = 100;
  int d = 0;
  
     for(int i=0;i<n;i++)
    {
       d = analogRead(A1);
      ph = d*0.015282730514518592  + 1.5012735608762107; // ph1 
      //ph = d*0.01488095238095238 + 1.626488095238095;//ph2
      
      //ph = d;//0.017917*d + 0.67712037 + 0.22;
      phM = phM+ph;
      delay(10);
    }

  return (phM/n);
 }
