#include "AD9833.h"     // Include the library

#define FNC_PIN 4       // Can be any digital IO pin
#define UD1 8    // pin  Generador
#define INC1 9   // pin  Generador
#define CS1 10    // pin  Generador


#define UD2 5    // pin Amplificador 
#define INC2 6   // pin Amplificador
#define CS2 7   // pin Amplificador  

AD9833 gen(FNC_PIN);       // Defaults to 25MHz internal reference frequency

// datos del serial
char dato=  ' ' ;
String datos="";
volatile int inicio=0;

// datos Configuracion
volatile float fmuestra=1; 
volatile float Voltaje=1.04;
volatile uint16_t pasos=40; 


void setup() 
{
    pinMode(CS1,OUTPUT);
    pinMode(UD1,OUTPUT);
    pinMode(INC1,OUTPUT);

    pinMode(CS2,OUTPUT);
    pinMode(UD2,OUTPUT);
    pinMode(INC2,OUTPUT);

    pinMode(3,OUTPUT);
    digitalWrite(3,LOW);
    digitalWrite(CS1,LOW);
    digitalWrite(CS2,LOW);
    
    digitalWrite(UD1,LOW);
    digitalWrite(UD2,LOW);

    
    puesta_cero_1();
    puesta_cero_2();
    set_paso(20,UD1,INC1);
    set_paso(20,UD2,INC2);

    gen.Begin();  
    gen.ApplySignal(SINE_WAVE,REG0,1000);
    gen.EnableOutput(true); // false
    Serial.begin(9600);
} 

void puesta_cero_1()
{
  digitalWrite(UD1,LOW);
  for(int i=0;i<=100;i++)
  {
      digitalWrite(INC1,HIGH);
      digitalWrite(INC1,LOW);
  }
      
}

void puesta_cero_2()
{
  digitalWrite(UD2,LOW);
  for(int i=0;i<=100;i++){
      digitalWrite(INC2,HIGH);
      digitalWrite(INC2,LOW);
  }    
}

void set_paso(int paso,int ud,int inc)
{
    if(ud==UD1){puesta_cero_1();}
    if(ud==UD2){puesta_cero_2();}
    digitalWrite(ud,HIGH);
    for(int i=1;i<=paso;i++){
      digitalWrite(inc,HIGH);
      digitalWrite(inc,LOW);
  }
}


void setFrecuenciaGenerador(String vector)
{
  String f = "";
  String frecuencia = "";
  String pasoSP = "";
  int j=0;
  while(j<vector.length())
  {
    if (vector[j]=='z'){frecuencia=f;f="";fmuestra = frecuencia.toInt();} 
    if (vector[j]=='g'){pasoSP=f;f="";j++;pasos = pasoSP.toInt();}
    f = f + vector[j];
    j++;
    //if (vector[j]=='d'){f="";}
  }
  set_paso(pasos,UD1,INC1);
  set_paso(pasos,UD2,INC2);
 
  gen.ApplySignal(SINE_WAVE,REG0,fmuestra);
  gen.EnableOutput(true); 
}


void iniciar()
{
    
    while(Serial.available()>0)
  {
      dato = Serial.read();
      if(dato!='d')
      {
        datos = datos +dato;
      }
      if(dato=='d')
      { 
        digitalWrite(3,HIGH);
        pasos=datos.toInt();
        set_paso(pasos,UD1,INC1);
        set_paso(pasos,UD2,INC2);
        //delay(100);
        Serial.println("y");
        datos="";
        dato= ' ';
        inicio=1;
        return;
      }
      
  } 
}

void loop() 
{
 
  while(inicio==0)
  {
    iniciar();
  }

  while(inicio==1)
  {     
      while(Serial.available()>0)
      {
        
        dato = Serial.read();
        datos = datos +dato;
        if (dato=='z'){setFrecuenciaGenerador(datos);datos="";}
        if (dato=='E'){ digitalWrite(3,LOW);inicio = 0;datos="";}
      }
  }

}
