from os import path

import datetime 

import xlrd 
  
# Give the location of the file 
loc = ('D:/alejandro/ProyectofGradute/mainProyect/impedance-project/pruevas/cafe_0/phcafe0.xlsx') 
  
# To open Workbook 
wb = xlrd.open_workbook(loc) 
sheet = wb.sheet_by_index(0) 

date1 = sheet.cell_value(2,1)
date2 = sheet.cell_value(3,1)
date3 = sheet.cell_value(4,1)
date = [] 
t0 = datetime.datetime(2020,9,7)
for i in range(1,28):
    d = sheet.cell_value(i,1)
    #datetime.timedelta(days=d)
    date.extend([t0+datetime.timedelta(days=d)])


def dataView(f,y):
    #x1 = []
    y1 = []
    f1 = []
    for i in range(2,len(y)-2):
        #x1.extend([(x[i-1] + x[i] + x[i+1])/3])
        y1.extend([(y[i-2]+y[i-1] + y[i] + y[i+1] +y[i+2])/5])
        f1.extend([f[i]])
    return [f1,y1]
    

#return 0
def loadCorreTxt(pathfileload):
    #print(pathfileload) 
    f = open(pathfileload,"r")
    matriz = []
    readed =  False
    datap = f.readline()
    
    while(readed == False):
        data = f.readline()
        fil = data.split(" ")
        fil  = fil[0:len(fil)-1]
        filnew = [] 
        for d in fil:
            if((d.isspace()==False)and(d!='')):
                #print(d)
                filnew.extend([float(d)])
        if(filnew==[]):
            readed = True
        if(filnew!=[]):
            matriz.extend([filnew])

    return matriz


import numpy as np
import matplotlib.pyplot as plt
import math


#pathfileload  = "D:/alejandro/ProyectofGradute/mainProyect/impedance-project/pruevas/aguaLimon_2/"

filename = "cafe_{}.txt"
#filename1 = "cafe_1.txt"
#filename2 = "cafe_2.txt"

Z  = []
XR = []
XI = []
FASE = []
for j in range(50):
    sp = 0.1
    Z1=[]
    xr1=[]
    xi1=[]
    fase =[]
    path1 = 'D:/alejandro/ProyectofGradute/mainProyect/impedance-project/pruevas/cafe_0/'
    if(path.exists(path1+filename.format(j))):
        M1 = loadCorreTxt(path1+filename.format(j))
        #M2 = loadCorreTxt(pathfileload2)
        [f,fase,amp,ampr] = np.transpose(M1)
        #[f1,fase1,amp1,ampr1] = np.transpose(M2)
        spv = np.ones(len(f))*sp
        for i in range(len(fase)):
            z = ampr[i]/(amp[i]/10000) # G =1000008
            Z1.extend([z])
            xr1.extend([z*math.cos(-math.radians(fase[i]))+0 ]) # calibracion
            xi1.extend([-z*math.sin(-math.radians(fase[i]))])

    Z.extend([Z1])
    XR.extend([xr1])
    XI.extend([xi1])
    FASE.extend([fase])
#plt.plot(f,fase)
#plt.plot(f,ampr)
#plt.plot(f,spv)
#plt.plot(f,amp)
#plt.plot(f,XR[0],'b')

fig, ax = plt.subplots()  # Create a figure and an axes.
#ax.plot(f,FASE[0],'b',label='0mm')
t0 = datetime.datetime(2019,9,6,7)
dt  = datetime.timedelta(minutes=30)
for j in range(1,28):
    i = j*2 + 30
    if(FASE[j]!=[]):
        [f1,x1] = dataView(f,FASE[j])
        #[f1,x1] = dataView(f[80:int(len(f)*1)],FASE[j][80:int(len(f)*1)])
        min1 = min(x1)
        i1  = x1.index(min1)
        print(x1[i1])
        #ax.plot(f1[i1][int(0.5*len(f1)):len(f1)],x1[i1][int(len(f1)*0.5):len(f1)],'*r')
        
        ax.plot(f1,x1,label=' {} '.format((date[j]).time()))
        #ax.plot(f[int(len(f)*0.5):len(f)],FASE[j][int(len(f)*0.5):len(f)],label=' {} '.format((date[j]).time()))

#ax.plot(f,FASE[6],'g',label='time 2 ')
#ax.plot(f,FASE[3],'m',label='time 3 ')
#ax.plot(f,FASE[7],'r',label='time 4 ')
#ax.plot(f,FASE[8],'b',label='time 5 ')
#ax.plot(f,XI[4],'r',label='800mm^3')

ax.set_xlabel('frecuencia [Hz]')  # Add an x-label to the axes.
ax.set_ylabel('fase [ ° ]')  # Add a y-label to the axes.
ax.set_title('cafe sin pulpa fermentando (20 litros) {}'.format(t0.date()))  # Add a title to the axes.

ax.legend()  # Add a legend.
plt.show()


