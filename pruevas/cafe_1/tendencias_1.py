import numpy as np
import math
import matplotlib.pyplot as plt
import sys


def get_r2_python(x_list, y_list):
       n = len(x_list)
       x_bar = sum(x_list)/n
       y_bar = sum(y_list)/n
       x_std = math.sqrt(sum([(xi-x_bar)**2 for xi in x_list])/(n-1))
       y_std = math.sqrt(sum([(yi-y_bar)**2 for yi in y_list])/(n-1))
       zx = [(xi-x_bar)/x_std for xi in x_list]
       zy = [(yi-y_bar)/y_std for yi in y_list]
       r = sum(zxi*zyi for zxi, zyi in zip(zx, zy))/(n-1)
       return r**2

def getPHT(may1):
    ph = []
    for i in may1:
        phT2  =  7.35157 - 0.08405*i
        ph.extend([phT2])
    return ph
        
    

data = [

[-8.0438,62473.6363,34274.0533,28.3308],

[-162962.18854,58207.6562,3.52245E6,162983.2101],

[-45.0821,62948.0133,56216.2844,70.7397],

[-31.7808,61985.2450,48957.5171,58.8524],

[-36.3350,62928.2482,51477.6136,65.1948],

[-10.0864,62660.2300,38309.1240,39.4800],

    ]


u = [-95745.0920,58240.8326,2692340.0000,95766.1468]
x = np.arange(10000,110000,1000)

#y0 = data[10][0]
#A1 = data[10][1]
#t1 = data[10][2]

#y = A1*np.exp(-x/t1) + y0
#plt.plot(x,y)
#plt.show()
 
may1 = []
ejex = list()
for i in range(len(data)):
#for i in range(9,17):
    y0 = float(data[i][0])
    xc = float(data[i][1])
    w  = float(data[i][2])
    A  = float(data[i][3])
    
    y = y0+A*np.exp(-0.5*((x-xc)/w)**2)
    max1 = max(y)
    ind = np.where(y == max1)
    may1.extend([max1])
    d = x[ind]
    ejex.append(d)
 
    plt.plot(x,y)

#max1 = getData(indices)


#print(may1)
ph = [5.68,5.5,5.41,4.9,4.95,4.87]




fig, axs = plt.subplots(2, 1)


t = ['16:00:00','17:00:00','18:23:00','19:26:00','20:13:00','21:19:00']

axs[0].plot(t,ph)
axs[1].plot(t,may1)

axs[0].grid()
axs[1].grid()


axs[0].set_ylabel(' ph ')
axs[1].set_ylabel(' maximos ')

fig, ax = plt.subplots()
x = np.arange(18,30,0.2)

phT = 7.35157 - 0.08405*x

phT2  =  getPHT(may1)

r2 = get_r2_python(ph,phT2)
print(r2)

ax.plot(may1,ph,'*c')
ax.plot(x,phT,'r',label='linea de tendencia')
ax.set_xlabel( ' maximos ' )
ax.set_ylabel(' ph ')
ax.set_title(' coeficiente r^2 = {}'.format(r2))
ax.grid()
ax.legend()  # Add a legend.
plt.show()
    
    
 











    


