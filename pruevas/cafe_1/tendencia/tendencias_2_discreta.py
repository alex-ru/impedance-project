import numpy as np
import math
import matplotlib.pyplot as plt
import sys
import xlrd 

def get_r2_python(x_list, y_list):
       n = len(x_list)
       x_bar = sum(x_list)/n
       y_bar = sum(y_list)/n
       x_std = math.sqrt(sum([(xi-x_bar)**2 for xi in x_list])/(n-1))
       y_std = math.sqrt(sum([(yi-y_bar)**2 for yi in y_list])/(n-1))
       zx = [(xi-x_bar)/x_std for xi in x_list]
       zy = [(yi-y_bar)/y_std for yi in y_list]
       r = sum(zxi*zyi for zxi, zyi in zip(zx, zy))/(n-1)
       return r**2
    
def getPHT(may1):
    ph = []
    for i in may1:
        phT2  =  -0.1355*i + 9.4466
        ph.extend([phT2])
    return ph

loc = ('D:/alejandro/ProyectofGradute/mainProyect/impedance-project/pruevas/cafe_1/tendencia/tendencias.xlsx') 
  
# To open Workbook 
wb = xlrd.open_workbook(loc) 
sheet = wb.sheet_by_index(0) 

print(sheet.nrows)
data = [] 
 
for i in range(5):
    i = i*4
    y0 = sheet.cell_value(i,11)
    xc = sheet.cell_value(i+1,11)
    w  = sheet.cell_value(i+2,11)
    A  = sheet.cell_value(i+3,11)
    d = [y0,xc,w,A]
    data.extend([d])
#print(data)

 
#sys.exit()
x = np.arange(10000,110000,1000)

#y0 = data[10][0]
#A1 = data[10][1]
#t1 = data[10][2]

#y = A1*np.exp(-x/t1) + y0
#plt.plot(x,y)
#plt.show()
may1 = []
ejex = list()
for i in range(len(data)):
#for i in range(9,17):
    y0 = data[i][0]
    xc = data[i][1]
    w  = data[i][2]
    A  = data[i][3]
    
    
    #print(d)
    y = y0+A*np.exp(-0.5*((x-xc)/w)**2)
    max1 = max(y)
    ind = np.where(y == max1)
    may1.extend([max1])
    d = x[ind]
    ejex.append(d)
    #y = A*np.exp(-x/t1) + y0
    plt.plot(x,y)

ph = [4.78,4.73,4.22,4.21,4.2]
plt.title('Tendencias de la fase ')
plt.xlabel('frecuencia ( Hz )')
plt.ylabel('fase ( ° )')

print(may1)
plt.plot(ejex,may1,'*k')
plt.grid()

 
fig, axs = plt.subplots(2, 1)


t = ['5:30:00','6:40:00','9:35:00','10:32:00','11:30:00']
import datetime 
tiempo = list()
for ti in t:
    t1 = datetime.datetime.strptime(ti, '%H:%M:%S')#.time()
    tiempo.append((t1))
    print(t1)


axs[0].plot(t,ph)
axs[1].plot(t,may1)

axs[0].grid()
axs[1].grid()


axs[0].set_ylabel(' ph ')
axs[1].set_ylabel(' maximos ')

fig, ax = plt.subplots()
x = np.arange(34,39,0.1)
phT = -0.1355*x + 9.4466

phT2  =  getPHT(may1)

r2 = get_r2_python(ph,phT2)

ax.plot(may1,ph,'*c')
ax.plot(x,phT,'r',label='linea de tendencia')
ax.set_xlabel( ' maximos ' )
ax.set_ylabel(' ph ')
ax.set_title(' coeficiente r^2 = {}'.format(r2))
ax.grid()
ax.legend()  # Add a legend.
plt.show()
    
    











    


