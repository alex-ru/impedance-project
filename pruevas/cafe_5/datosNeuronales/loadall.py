from os import path
import os
from os import listdir
import datetime 
import numpy as np
import matplotlib.pyplot as plt
import math
import sys 

def seStyle(ax):
    ax.tick_params(axis='both', which='major', labelsize=14)     

    ax.locator_params(axis='x', nbins=10, tight=True)
    ax.locator_params(axis='y', nbins=6, tight=True)


    #ax.legend(title="Concentración", loc='upper left')  # Add a legend.
    y_offset = ax.yaxis.get_offset_text()
    y_offset.set_size(18)
    t = ax.xaxis.get_offset_text()
    t.set_size(18)
    
    x_offset = ax.xaxis.get_offset_text()
    x_offset.set_size(18)
    tx = ax.xaxis.get_offset_text()
    tx.set_size(18)
    
    ax.grid(b=True, which='major', axis='both', alpha=.5)


def plotLines(P,B):
    fig, ax  = plt.subplots()
    f = np.arange(10000,200000)
    print("entró")
    for i in range(len(P)):
        fase = P[i]*f + B[0]
        ax.plot(f,fase)
    seStyle(ax)
    #ax.set_ylim(0.7,1.2)
    ax.set_xlabel('frecuencia [Hz]')  # Add an x-label to the axes.
    ax.set_ylabel('FASE [ ° ]')  # Add a y-label to the axes.
    ax.set_title("Tendencias prueba #1")  # Add a title to the axes.
    #ax.legend()  # Add a legend.
    #ax.title("Tendencias prueba #3")
    #ax.xlabel('frecuencia [ Hz ]')
    #ax.ylabel('fase [ ° ]')
    plt.show()
    
def loadTxt(pathfileload):
    
    f = open(pathfileload,"r")
    matriz = []
    readed =  False
    datap = f.readline()
    while(readed == False):
        data = f.readline()
        fil = data.split(" ")
        fil  = fil[0:len(fil)-1]
        filnew = [] 
        for d in fil:
            if(d!=''):
                #print(d)
                filnew.extend([float(d)])
        if(filnew==[]):
            readed = True
        if(filnew!=[]):
            matriz.extend([filnew])

    return matriz
def ordenar(lista):
    new_list = []
    for i in range(len(lista)):
            minimo = min(lista)
            new_list.extend([minimo])
            lista.remove(minimo)
            
    new_list = new_list
    return new_list

def getFileListImp(path,variedad):
    lista = []
    dates = []
    if os.path.isdir(path): # se la ruta existe 
            listfiles = listdir(path) # lista de archivos dentro de esa ruta        
            for file in listfiles:
                if(("txt" in file)and("cafe" in file)):
                    lista.extend([file])
                    [day,hour,minute] = file[0:len(file)-4].split("_")[2:]
                    d = datetime.datetime(2020,10,int(day),int(hour),int(minute))
                    dates.extend([d])
                    #print(file)
    dates = ordenar(dates)
    lista = []
    for d in dates:
        file = "cafe_"+variedad +"_{}_{}_{}.txt".format(d.day,d.hour,d.minute)
        lista.extend([file])
    return lista,dates

def convert(datos):
    ph = []
    humedad = []
    tempcoffe = []
    tempAmb = []
    tiempo  = [] 
    
    for j in range(len(datos[0])):
        tempAmb.extend(  [float(datos[2][j])])
        tempcoffe.extend([float(datos[3][j])])
        ph.extend(       [float(datos[4][j])])
        humedad.extend(  [float(datos[5][j])])

        dato = datos[0][j] +' '+ datos[1][j]
        d1 = dato.split(".")[0]
        t1 = datetime.datetime.strptime(d1, '%Y-%m-%d %H:%M:%S')
        tiempo.extend([t1])
        
    return [ph,humedad,tempcoffe,tempAmb,tiempo]

def getFileList(path):
    if os.path.isdir(path): # se la ruta existe 
            listfiles = listdir(path) # lista de archivos dentro de esa ruta
            lista = []
            for file in listfiles:
                if(("txt" in file)and("cafe" in file)):
                    lista.extend([file])
                    print(file)
    return lista

def dataView(f,y,n=10):
    y1 = []
    f1 = []
    for i in range(n,len(y)-n):
        yd = 0
        for j in range(-n,n):
            yd = y[i-j] + yd
        yd = yd/((2*n)+1)
        y1.extend([yd])
        #x1.extend([(x[i-1] + x[i] + x[i+1])/3])
        #y1.extend([(y[i-2]+y[i-1] + y[i] + y[i+1] +y[i+2])/5])
        f1.extend([f[i]])
    return [f1,y1]

def getMePath():
    path = __file__
    #print("aqui esta el path")
    #print(__file__)
    path = path.split('\\')
    p=''
    for i in range(len(path)-1):
        p += (path[i]+'/')
    # print(p)
    return(p)

def dataView(f,y,n=10):
    y1 = []
    f1 = []
    for i in range(n,len(y)-n):
        yd = 0
        for j in range(-n,n):
            yd = y[i-j] + yd
        yd = yd/((2*n)+1)
        y1.extend([yd])
        #x1.extend([(x[i-1] + x[i] + x[i+1])/3])
        #y1.extend([(y[i-2]+y[i-1] + y[i] + y[i+1] +y[i+2])/5])
        f1.extend([f[i]])
    return [f1,y1]

def exportCVS(dat):
        import pandas as pd
        path= "D:/alejandro/ProyectofGradute/mainProyect/impedance-project/pruevas/cafe_5/datosNeuronales"#getMePath()
        path =  "D:/alejandro/ProyectofGradute/mainProject/impedance-proyect/pruevas/cafe_5/datosNeuronales"
        #filename = "cafe_{}.txt".format(n)
        #fileSave = open (ruta+filename,'w')
        datos = {
        'tiempo': dat[0],
        'temp': dat[1],
        'humedad': dat[2],
        'pH': dat[3],
        'pendiente': dat[4],
        'corte': dat[5]
        }

        df = pd.DataFrame(datos, columns= ['tiempo', 'temp','humedad','pH','pendiente','corte'])
        df.to_csv (path+'/'+'v_colombia18octubre.csv', index = False, header=True)
    
        #print (df)
        print("datos exportados csv")
        
def loadTxtSensor(pathfileload):

    f = open(pathfileload,"r")
    matriz = []
    readed =  False
    datap = f.readline()
    while(readed == False):
        data = f.readline()
        fil = data.split(" ")
        fil  = fil[0:len(fil)-1]
        filnew = [] 
        for d in fil:
            if(d!=''):
                #print(d)
                #if("2020" in d):
                    #filnew.extend([d])
                #if(":" in d):
                    filnew.extend([str(d)])
                #if((("2020" in d)==False)and((":" in d))==False):
                    #filnew.extend([float(d)])
    
        if(filnew==[]):
            readed = True
        if(filnew!=[]):
            matriz.extend([filnew])

    return matriz

def minimos(x,y):
    from sklearn.linear_model import LinearRegression
    from sklearn.metrics import mean_squared_error

    regresion_lineal = LinearRegression()
    regresion_lineal.fit(x.reshape(-1,1), y)

    prediccion_entrenamiento = regresion_lineal.predict(x.reshape(-1,1))

    mse = mean_squared_error(y_true = y, y_pred = prediccion_entrenamiento)
    rmse = np.sqrt(mse)
    r2 = regresion_lineal.score(x.reshape(-1,1), y)
    
    m = regresion_lineal.coef_[0]
    b = regresion_lineal.intercept_
    
    #y1  =m*x + b
    #plt.plot(x,y)
    #plt.plot(x,y1)
    #plt.show()
    print('Error Cuadrático Medio (MSE) = ' + str(mse))
    print('Raíz del Error Cuadrático Medio (RMSE) = ' + str(rmse))
    print('w = ' + str(regresion_lineal.coef_) + ', b = ' + str(regresion_lineal.intercept_))
    print('Coeficiente de Determinación R2 = ' + str(r2))
    return[m,b,mse,rmse,r2]

def getDataSensorTo(min1,S):
    
    [ph,humedad,tempcoffe,tempAmb,tiempo] = S
    t0 = tiempo[0]
    ph_new = []
    humedad_new = []
    tempAmb_new = []
    tiempo_new  = []
    for i in range(len(tiempo)):
        if ((tiempo[i].minute==min1)and(tiempo[i].hour!=8)):
            print(tiempo[i])
            ph_new.extend([ph[i]])
            humedad_new.extend([humedad[i]])
            tempAmb_new.extend([tempAmb[i]])
            tiempo_new.extend([int(((tiempo[i]-t0).total_seconds())/3600)])
    return [ph_new,humedad_new,tempAmb_new,tiempo_new]
    
def getImpedanceData():
    Z    = []
    XR   = []
    XI   = []
    FASE = []
    f    = []
    path= "D:/alejandro/ProyectofGradute/mainProyect/impedance-project/pruevas/cafe_5/datosNeuronales"#getMePath()
    path =  "D:/alejandro/ProyectofGradute/mainProject/impedance-proyect/pruevas/cafe_5/datosNeuronales"
    lista,dates = getFileListImp(path,"colombia")
    #print("path : " + str(path))
    #print(lista)
    
    M = loadTxt(path+'/'+"test_0.txt")
    [f0,fase0,amp0,ampr0] = np.transpose(M)
    
    for j in range(len(lista)):
        sp = 0.1
        Z1=[]
        xr1=[]
        xi1=[]
        fase =[]
        M1 = loadTxt(path+'/'+lista[j])
        [f,fase,amp,ampr] = np.transpose(M1)
        fase = fase - fase0
        spv = np.ones(len(f))*sp
        
        for i in range(len(fase)):
            z = ampr[i]/(amp[i]/10000) # G =1000008
            Z1.extend([z])
            xr1.extend([z*math.cos(-math.radians(fase[i]))+0 ]) # calibracion
            xi1.extend([z*math.sin(-math.radians(fase[i]))])

        Z.extend([Z1])
        XR.extend([xr1])
        XI.extend([xi1])
        FASE.extend([fase])

    return [f,Z,XR,XI,FASE]

path= "D:/alejandro/ProyectofGradute/mainProyect/impedance-project/pruevas/cafe_5/datosNeuronales"#getMePath()
path =  "D:/alejandro/ProyectofGradute/mainProject/impedance-proyect/pruevas/cafe_5/datosNeuronales"

[f,Z,XR,XI,FASE] = getImpedanceData()
#[m,b,mse,rmse,r2] = minimos(f,FASE[0])
Pendiente = []
B = []
for f1 in FASE:
    [m,b,mse,rmse,r2] = minimos(f,f1)
    Pendiente.extend([m])
    B.extend([b])
    
#path1 = getMePath()
M = loadTxtSensor(path+'/'+"datosSensores_5.txt")
M1 = np.transpose(M)
[ph,humedad,tempcoffe,tempAmb,tiempo] = convert(M1)
[ph_new,humedad_new,tempAmb_new,tiempo_new] = getDataSensorTo(58,[ph,humedad,tempcoffe,tempAmb,tiempo])
print(tiempo_new)
print("ph   : "+str(len(ph_new)))
print("fase : "+str(len(Pendiente)))
plotLines(Pendiente,B)
sys.exit()
exportCVS([tiempo_new,tempAmb_new,humedad_new,ph_new,Pendiente,B])
sys.exit()

fig, axs = plt.subplots(3, 1)
axs[0].plot(tiempo,ph,label='ph')
axs[1].plot(tiempo,humedad,label='humedad')
axs[2].plot(tiempo,tempAmb,label='T° ambiente')
#axs[2].plot(tiempo,tempcoffe,label='T° cafe')

axs[0].set_ylim(3.5,7)
axs[1].set_ylim(70,100)
axs[2].set_ylim(15,32)

axs[2].set_xlabel('tiempo')  # Add an x-label to the axes.
axs[0].set_ylabel('pH')  # Add a y-label to the axes.
axs[1].set_ylabel('humedad [%]')  # Add a y-label to the axes.
axs[2].set_ylabel('Temperaturas [ °C ]')  # Add a y-label to the axes.
axs[0].set_title('cafe sin pulpa fermentando (1 Kg variedad costarica)')  # Add a title to the axes.
#ax.set_title('cafe sin pulpa fermentando (20 litros) {}'.format(t0.date()))  # Add a title to the axes.

axs[0].legend()  # Add a legend.
axs[1].legend()  # Add a legend.
axs[2].legend()  # Add a legend.

axs[0].grid()  # Add a legend.
axs[1].grid()  # Add a legend.
axs[2].grid()  # Add a legend.

plt.show()
