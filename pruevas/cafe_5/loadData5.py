from os import path
import os
from os import listdir
import datetime 
import numpy as np
import matplotlib.pyplot as plt
import math
import sys 

def seStyle(ax):
    ax.tick_params(axis='both', which='major', labelsize=14)     

    ax.locator_params(axis='x', nbins=10, tight=True)
    ax.locator_params(axis='y', nbins=6, tight=True)


    #ax.legend(title="Concentración", loc='upper left')  # Add a legend.
    y_offset = ax.yaxis.get_offset_text()
    y_offset.set_size(18)
    t = ax.xaxis.get_offset_text()
    t.set_size(18)
    
    x_offset = ax.xaxis.get_offset_text()
    x_offset.set_size(18)
    tx = ax.xaxis.get_offset_text()
    tx.set_size(18)
    
    ax.grid(b=True, which='major', axis='both', alpha=.5)


def getFileList(path):
    if os.path.isdir(path): # se la ruta existe 
            listfiles = listdir(path) # lista de archivos dentro de esa ruta
            lista = []
            for file in listfiles:
                if(("txt" in file)and("cafe" in file)):
                    lista.extend([file])
                    print(file)
    return lista

def getMePath():
    path = __file__
    path = path.split('\\')
    p=''
    for i in range(len(path)-1):
        p += (path[i]+'/')
    if(p==''):
        p =  "D:/alejandro/ProyectofGradute/mainProject/impedance-proyect/pruevas/cafe_5/"
        return p
    else:
        return(p)

def dataView(f,y,n=10):
    y1 = []
    f1 = []
    for i in range(n,len(y)-n):
        yd = 0
        for j in range(-n,n):
            yd = y[i-j] + yd
        yd = yd/((2*n)+1)
        y1.extend([yd])
        #x1.extend([(x[i-1] + x[i] + x[i+1])/3])
        #y1.extend([(y[i-2]+y[i-1] + y[i] + y[i+1] +y[i+2])/5])
        f1.extend([f[i]])
    return [f1,y1]

def exportTxt(m,filename):
        #ruta = "D:/alejandro/ProyectofGradute/mainProyect/impedance-project/pruevas/cafe_1/tendencia/"
        #filename = "cafe_{}.txt".format(n)
        fileSave = open (ruta+filename,'w')
        fileSave.write(" f(hz)      Fase(°)     XR(ohm)      XI(ohm)   ")    
        fileSave.write('\n')
        for i in range(len(m[0])):
            for j in range(len(m)):
                dato = "{0:.11f}".format(m[j][i])
                if (m[j][i]>0):
                    fileSave.write(dato[0:8])
                else:
                    fileSave.write(dato[0:8])
                fileSave.write("   ")
            fileSave.write('\n')
        fileSave.close()

        print("funcion para exportar txt")
        
def loadTxt(pathfileload):

    f = open(pathfileload,"r")
    matriz = []
    readed =  False
    datap = f.readline()
    while(readed == False):
        data = f.readline()
        fil = data.split(" ")
        fil  = fil[0:len(fil)-1]
        filnew = [] 
        for d in fil:
            if(d!=''):
                #print(d)
                filnew.extend([float(d)])
        if(filnew==[]):
            readed = True
        if(filnew!=[]):
            matriz.extend([filnew])

    return matriz

path= getMePath()
lista = getFileList(path)

M = loadTxt(path+'/'+"test_0.txt")
[f0,fase0,amp0,ampr0] = np.transpose(M)


Z  = []
XR = []
XI = []
FASE = []

for j in range(len(lista)):
    sp = 0.1
    Z1=[]
    xr1=[]
    xi1=[]
    fase =[]
    
    M1 = loadTxt(path+'/'+lista[j])
    [f,fase,amp,ampr] = np.transpose(M1)
    fase = fase - fase0
    spv = np.ones(len(f))*sp
    
    for i in range(len(fase)):
        z = ampr[i]/(amp[i]/10000) # G =1000008
        Z1.extend([z])
        xr1.extend([z*math.cos(-math.radians(fase[i]))+0 ]) # calibracion
        xi1.extend([z*math.sin(-math.radians(fase[i]))])

    Z.extend([Z1])
    XR.extend([xr1])
    XI.extend([xi1])
    FASE.extend([fase])

fig, ax = plt.subplots()  # Create a figure and an axes.

tiempo = '18_{}:10'
for j in range(len(FASE)):
    #i = j*2 + 30
    if(FASE[j]!=[]):
        
        ni = int(len(f)*0)
        nf = int(len(f)*1)
        
        [f1,fase1] = dataView(f[ni:nf],FASE[j][ni:nf])
        [xr1,xi1] = dataView(XR[j][ni:nf],XI[j][ni:nf])
        [z1,z1] = dataView(Z[j][ni:nf],Z[j][ni:nf])
        #exportTxt([f1,x1,xr1,xi1],'cafe_{}.txt'.format(j))
        #fileSave.write(" f(hz)      Fase(°)     XR(ohm)      XI(ohm)    Z (ohm)")    
        #[f1,x1] = dataView(f[80:int(len(f)*1)],FASE[j][80:int(len(f)*1)])
        min1 = min(fase1)
        i1  = fase1.index(min1)
        print(fase1[i1])
        
        ax.plot(f1,fase1,label='18_{}:10'.format((j+10)))

seStyle(ax)
ax.set_xlabel('frecuencia [Hz]')  # Add an x-label to the axes.
ax.set_ylabel('fase [ ° ]')  # Add a y-label to the axes.
ax.set_title('cafe sin pulpa fermentando (1Kg variedad Colombia)')  # Add a title to the axes.
#ax.set_title('cafe sin pulpa fermentando (20 litros) {}'.format(t0.date()))  # Add a title to the axes.

ax.legend()  # Add a legend.
plt.show()


