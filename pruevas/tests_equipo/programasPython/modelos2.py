import numpy as np 
import matplotlib.pyplot as plt
import math
# modelo  R - (R||C)

p = 0.000000000001
n = 0.000000001
u = 0.000001

def modelo1(color):
    
    R1 = 33000
    R2 = 100000
    C1 =  100*p
    Zr = []
    Zi = []
    Fr = []
    Z  = []
    fase = []

    for f in range(10000,200000,1000):
        
        WC1 = -1j/(2*math.pi*f*C1)
        z =   R1 + ((R2*WC1)/(R2+WC1))
        Z.extend([z])
        Fr.extend([f])
        Zr.extend([z.real])
        Zi.extend([-z.imag])
        fase.extend([math.degrees(math.atan2(-z.imag,z.real))])

    #plt.figure(2)
    plt.plot(Zr,Zi,color)
    #plt.plot(Zr1,Zi1,'-')
    #plt.plot(Fr,fase,'-')
    plt.grid()
    #plt.plot(Fr,fase,'*')

def modelo2(color):
    
    R1 = 20000
    R2 = 100000
    C1 =  100*p
    
    Zr = []
    Zi = []
    Fr = []
    Z  = []
    fase = []

    for f in range(10000,200000,1000):
        
        WC1 = -1j/(2*math.pi*f*C1)
        z =   R1 + ((R2*WC1)/(R2+WC1))
        Z.extend([z])
        Fr.extend([f])
        Zr.extend([z.real])
        Zi.extend([-z.imag])
        fase.extend([math.degrees(math.atan2(-z.imag,z.real))])

    #plt.figure(2)
    plt.plot(Zr,Zi,color)
    #plt.plot(Zr1,Zi1,'-')
    #plt.plot(Fr,fase,'-')
    plt.grid()
    #plt.plot(Fr,fase,'*')

modelo1('*r')
modelo2('b')
plt.show()
