import matplotlib.pyplot as plt
import math

def get_r2_python(x_list, y_list):
       n = len(x_list)
       x_bar = sum(x_list)/n
       y_bar = sum(y_list)/n
       x_std = math.sqrt(sum([(xi-x_bar)**2 for xi in x_list])/(n-1))
       y_std = math.sqrt(sum([(yi-y_bar)**2 for yi in y_list])/(n-1))
       zx = [(xi-x_bar)/x_std for xi in x_list]
       zy = [(yi-y_bar)/y_std for yi in y_list]
       r = sum(zxi*zyi for zxi, zyi in zip(zx, zy))/(n-1)
       return r**2
    
ST = [10,100,1000,10000,20000,30000,40000,50000,60000,70000,80000,90000,100000]
Real = [9.986,99.8, 998,10200,20200,30330,40065,50000,59715,70200,80000,89800,100000]

r2 = get_r2_python(ST,Real)
fig, ax = plt.subplots()  # Create a figure and an axes.


ax.plot(ST,'b',label='frecuencia deseada')
ax.plot(Real,'*k',label='frecuencia real ')
ax.set_xscale("log", nonposx='clip')
ax.set_ylim(bottom=0.1)
plt.grid()
ax.legend()  # Add a legend.
plt.title('Generacion de barrido de frecuencias r^2 = {}'.format(r2))
#plt.plot(Real,'*')
plt.show()





