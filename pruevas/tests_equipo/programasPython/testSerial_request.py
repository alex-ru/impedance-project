import serial
import time

def limpiar(s,ruido): # (str,"eliminar")
    DatoLimpio = ""
    for letra in s:
        if letra not in ruido:
            DatoLimpio += letra
    return DatoLimpio

arduino = serial.Serial('COM6',baudrate=9600,timeout=0)

i=0
datos = ''
while(i<20):
    while arduino.in_waiting:  # Or: while ser.inWaiting():        
        dato = str(arduino.read())
        dato = limpiar(dato,"b/rn'")
        datos = datos + dato
        if('c' in dato):
            print(datos)
            datos = ''
            i = i+1
arduino.close() 