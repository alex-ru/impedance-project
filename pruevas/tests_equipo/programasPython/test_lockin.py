import lockin
import numpy as np
import math
import matplotlib.pyplot as plt

def get_r2_python(x_list, y_list):
       n = len(x)
       x_bar = sum(x_list)/n
       y_bar = sum(y_list)/n
       x_std = math.sqrt(sum([(xi-x_bar)**2 for xi in x_list])/(n-1))
       y_std = math.sqrt(sum([(yi-y_bar)**2 for yi in y_list])/(n-1))
       zx = [(xi-x_bar)/x_std for xi in x_list]
       zy = [(yi-y_bar)/y_std for yi in y_list]
       r = sum(zxi*zyi for zxi, zyi in zip(zx, zy))/(n-1)
       return r**2
    
def exportTxt(filename,m):
        ruta = "D:/alejandro\ProyectofGradute/mainProyect/impedance-project/pruevas/test_Lockin/"
        #filename = "cafe_{}.txt".format(n)
        fileSave = open (ruta+filename,'w')
        fileSave.write(" Amp(v)   AmpR(v)     Fase(°)      A(v)      F(°)    fre(Hz)")    
        fileSave.write('\n')
        for i in range(len(m)):
            for j in range(len(m[0])):
                dato = "{0:.11f}".format(m[i][j])
                if (m[i][j]>0):
                    fileSave.write(dato[0:8])
                else:
                    fileSave.write(dato[0:8])
                fileSave.write("   ")
            fileSave.write('\n')
        fileSave.close()

        print("funcion para exportar txt")

#fig, ax = plt.subplots()

M = [] 
for i in range(1000,200000,1000):
    n = 100000
    fs = 1000000
    dt = 1/fs
    f = 10000
    fase = 30.0
    angulo = math.radians(fase)
    A = 1.0
    t = np.arange(0,n*dt,dt)
    #print(len(t))
    r = 2*A*np.random.random(len(t))

    x = A*np.sin((2*math.pi*f*t) + angulo) + r 
    y1 = np.sin((2*math.pi*f*t))
    y2 = np.sin((2*math.pi*f*t)+ math.radians(90))

    #ax.plot(t,x,'k',label='Señal a medir')
    #ax.plot(t,x-r,'-b',label='Señal orginal')
    #ax.legend()  # Add a legend.
    #plt.show()
    
    d = lockin.lockin(x,y1,fs,f)
    d.extend([A,fase,i])
    M.extend([d])
exportTxt('test_01.txt',M)

fig, ax = plt.subplots()

data =   np.transpose(M, axes=None)


r2_1 = get_r2_python(data[0],data[3])
r2_2 = get_r2_python(data[2],data[4])
Amedida = (data[0] - 1)*100
Areal = data[0]  - 1 

ax.plot(data[5],Amedida,'k',label='amplitud medida')
ax.plot(data[5],Areal,'-b',label='amplitud real')
ax.set_title('Medicion de Amplitud mediante lock-in digital r^2 = {}'.format(r2_1))
#ax.set_xlim(0,200)
ax.set_ylabel('Amplitud ( v )')
ax.set_xlabel('frecuencia ( Hz )')
ax.grid()
#ax.set_ylim(0.9,1.1)
ax.legend()  # Add a legend.

plt.show()
import sys 
sys.exit()

fig, ax = plt.subplots()

#data =   np.transpose(M, axes=None)

ax.plot(data[5],data[2],'k',label='fase medida')
ax.plot(data[5],data[4],'-b',label='fase real')
ax.set_title('Medicion de Fase mediante lock-in digital r^2 = {}'.format(r2_2))
ax.set_ylabel('fase ( ° )')
ax.set_xlabel('frecuencia ( Hz )')
#ax.set_xlim(0,200)
ax.set_ylim(28,32)
ax.grid()
ax.legend()  # Add a legend.


#plt.plot(data[3])
#plt.plot(t,x)
#plt.plot(t,y1)
#plt.plot(t,y2)
plt.show()
    #angulo  = np.degrees(3.1416)
    #lockin(Signal,Vr,fs,f):
    #print(t)
