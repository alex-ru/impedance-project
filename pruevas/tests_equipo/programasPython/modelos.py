import numpy as np 
import matplotlib.pyplot as plt
import math
# modelo  R - (R||C)

p = 0.000000000001
n = 0.000000001
u = 0.000001


R1 = 20000
#R2 = 68000
#L = 100*u
C1 =  100*p
#C2 =  15*p
#C =  10*p

Zr = []
Zi = []
Fr = []
Z  = []
fase = []
#plt.plot(1000,R1,'*')
for f in range(1000,4000000,10000):
    #WL = 1j*(2*math.pi*f*L)
    WC1 = -1j/(2*math.pi*f*C1)
    #WC2 = -1j/(2*math.pi*f*C2)
    #z =  (((WC1*R1)/(WC1+R1)+R2)*WC2)/(((WC1*R1)/(WC1+R1)+R2)+WC2)
    z =  (WC1*R1)/(WC1+R1)
    Z.extend([z])
    Fr.extend([f])
    Zr.extend([z.real])
    Zi.extend([-z.imag])
    fase.extend([math.degrees(math.atan2(-z.imag,z.real))])
    
plt.plot(Zr,Zi,'*r')
#plt.plot(Zr1,Zi1,'-')
#plt.plot(Fr,fase,'-')
plt.grid()
#plt.plot(Fr,fase,'*')
plt.show()
