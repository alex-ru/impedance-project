from os import path
import datetime 
import sys

def inver(u):
    v = []
    n = len(u) - 1
    for i in range(n+1):
        v.extend([u[n-i]])
        #print(u[n-i])
    #sys.exit()
     
    return v

def dataView(f,y,n=10):
    #print(len(f))
    #print(len(y))
    #x1 = []
    y1 = []
    f1 = []
    for i in range(n,len(y)-n):
        yd = 0
        for j in range(-n,n):
            yd = y[i-j] + yd
        yd = yd/((2*n)+1)
        y1.extend([yd])
        #x1.extend([(x[i-1] + x[i] + x[i+1])/3])
        #y1.extend([(y[i-2]+y[i-1] + y[i] + y[i+1] +y[i+2])/5])
        f1.extend([f[i]])
    return [f1,y1]

def dataViewV(f,y,n=10):
    y1 = []
    f1 = []
    m = 0#sum(y)/len(y)
    for i in range(n,len(y)-n):
        yd = 0
        for j in range(-n,n):
            yd = y[i-j] + yd
        yd = yd/((2*n)+1)
        y1.extend([yd+m])
        f1.extend([f[i]])
    return [f1,y1]
        

#return 0

def exportTxt(m,filename):
        #ruta = "D:/alejandro/ProyectofGradute/mainProyect/impedance-project/pruevas/calibracion/pasos/"
        #filename = "cafe_{}.txt".format(n)
        fileSave = open (filename,'w')
        fileSave.write(" f(hz)      Fase(°)     VR(v)      VS(v)   ")    
        fileSave.write('\n')
        for i in range(len(m[0])):
            for j in range(len(m)):
                dato = "{0:.11f}".format(m[j][i])
                if (m[j][i]>0):
                    fileSave.write(dato[0:8])
                else:
                    fileSave.write(dato[0:8])
                fileSave.write("   ")
            fileSave.write('\n')
        fileSave.close()

        print("funcion para exportar txt")
        
def loadCorreTxt(pathfileload):

    f = open(pathfileload,"r")
    matriz = []
    readed =  False
    datap = f.readline()
    
    while(readed == False):
        data = f.readline()
        fil = data.split(" ")
        fil  = fil[0:len(fil)-1]
        filnew = [] 
        for d in fil:
            if(d!=''):
                #print(d)
                filnew.extend([float(d)])
        if(filnew==[]):
            readed = True
        if(filnew!=[]):
            matriz.extend([filnew])

    return matriz


import numpy as np
import matplotlib.pyplot as plt
import math


#pathfileload  = "D:/alejandro/ProyectofGradute/mainProyect/impedance-project/pruevas/aguaLimon_2/"

filename = "test_pasos_{}.txt"
#filename1 = "cafe_1.txt"
#filename2 = "cafe_2.txt"

F  = []
A1 = []
A2 = []
FASE = []
for j in range(10000,200000,20000):
    sp = 0.1
    path1 = 'D:/alejandro/ProyectofGradute/mainProyect/impedance-project/pruevas/calibracion/pasos/'
    if(path.exists(path1+filename.format(j))):
        M1 = loadCorreTxt(path1+filename.format(j))
        
        [f,fase,amp,ampr] = np.transpose(M1)
        spv = np.ones(len(f))*sp

        #plt.plot(f,fase)
        
        A1.extend([ampr])
        A2.extend([amp])
        FASE.extend([fase])
        
        print(len(f))
        print(len(fase))




#plt.plot(f,ampr)
#plt.show()
#plt.plot(f,spv)
#plt.plot(f,amp)
#plt.plot(f,XR[0],'b')

fig, ax = plt.subplots()  # Create a figure and an axes.
#ax.plot(f,FASE[0],'b',label='0mm')
t0 = datetime.datetime(2019,9,6,7)
dt  = datetime.timedelta(minutes=30)
#for j in range(13,20):


i = 190000
for j in range(len(A1)):
    
    if(FASE[j]!=[]):    
        ni = int(len(f)*0.0)
        nf = int(len(f)*0.99)

        [f1,x1] = dataViewV(f[ni:nf],A1[j][ni:nf])
        #[x1,nda] = dataView(A1[j][ni:nf],A1[j][ni:nf])
        [x2,fase1] = dataView(A2[j][ni:nf],FASE[j][ni:nf])
        #exportTxt([f1,fase1,x1,x2],'test_ten_{}.txt'.format(j))
        #fileSave.write(" f(hz)      Fase(°)     XR(ohm)      XI(ohm)    Z (ohm)")    
        #[f1,x1] = dataView(f[80:int(len(f)*1)],FASE[j][80:int(len(f)*1)])
        #min1 = min(x1)
        #i1  = x1.index(min1)
        #print(x1[i1])
        #ax.plot(f1[i1][int(0.5*len(f1)):len(f1)],x1[i1][int(len(f1)*0.5):len(f1)],'*r')
        pasos = FASE[j]
        Volt  =  A2[j]
        pasos = inver(pasos)
        #sys.exit()
        ax.plot(pasos,Volt,'-',label=' frecuencia {}'.format(i))
        i = i - 20000
        #ax.plot(f1,x1,'-',label='medicion {}'.format(j))
        #ax.plot(f[int(len(f)*0.5):len(f)],FASE[j][int(len(f)*0.5):len(f)],label=' {} '.format((date[j]).time()))

#ax.plot(f,FASE[6],'g',label='time 2 ')
#ax.plot(f,FASE[3],'m',label='time 3 ')
#ax.plot(f,FASE[7],'r',label='time 4 ')
#ax.plot(f,FASE[8],'b',label='time 5 ')
#ax.plot(f,XI[4],'r',label='800mm^3')

ax.set_xlabel('pasos')  # Add an x-label to the axes.
ax.set_ylabel('Amplitud del Voltaje Aplicado [ V ]')  # Add a y-label to the axes.
ax.set_title('Volataje amplicado a la muestra vs numero pasos del poteciometro a diferentes frecuencia (muestra de 68KOhms)')  # Add a title to the axes.
ax.grid()
#ax.set_title('cafe sin pulpa fermentando (20 litros) {}'.format(t0.date()))  # Add a title to the axes.

ax.legend()  # Add a legend.
plt.show()


