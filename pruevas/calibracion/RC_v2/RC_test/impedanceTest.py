import sys
from impedance import preprocessing
from impedance.models.circuits import CustomCircuit


import matplotlib.pyplot as plt
from impedance.visualization import plot_nyquist, plot_residuals
import numpy as np
import math
#circuit = 'R0-p(R1,C1)-p(R2,C2)'
#initial_guess = [.01, .01, 100, .01, 0.001]

def seStyle(ax,eje2=False,n=0):
    units='Ohms'
    unitsx = 'Hz'
    ax.tick_params(axis='both', which='major', labelsize=14)     

    ax.locator_params(axis='x', nbins=6, tight=True)
    ax.locator_params(axis='y', nbins=6, tight=True)

    if(n==0):
        ax.set_ylabel(r'$-Z^{\prime\prime}(\omega)$ ' +
                  '$[{}]$'.format(units), fontsize=14)
    if(n==1):
        ax.set_ylabel(r'$Z^{\prime}(\omega)$ ' +
                  '$[{}]$'.format(units), fontsize=14)
    if(n==2):
        ax.set_ylabel(r'$-Fase(\omega)$ ' +
                  '$[{}]$'.format(units), fontsize=14)
    if(n==3):
        ax.set_ylabel(r'$Z(\omega)$ ' +
                  '$[{}]$'.format(units), fontsize=14)
    if(eje2==True):
        ax.set_xlabel(r'$frecuencia $ ' +
                  '$[{}]$'.format(unitsx), fontsize=14)

    #ax.legend(title="Concentración", loc='upper left')  # Add a legend.
    y_offset = ax.yaxis.get_offset_text()
    y_offset.set_size(8)
    t = ax.xaxis.get_offset_text()
    t.set_size(18)
    ax.grid(b=True, which='major', axis='both', alpha=.5)

def getFase(Z):
    fase  = (180/math.pi)*np.arctan2(np.imag(Z),np.real(Z))
    return fase

def getMag(Z):
    mag  = np.sqrt( (np.imag(Z)**2) + (np.real(Z)**2) )
    return mag

p = 0.000000000001

R0 = 33000
R1 = 68000
C0 =  100*p
    
circuit = 'R1-p(R2,C)'
#circuit = 'R0-p(R1,C1)-p(R2-L0,C2)'
initial_guess = [R0, R1, C0]
#initial_guess = [.01, .01, 100, .01, .05, 100, 1]



circuit = CustomCircuit(circuit, initial_guess=initial_guess)


# Load data from the example EIS result
frequencies, Z = preprocessing.readCSV('./datosImpedance.csv')
# keep only the impedance data in the first quandrant
frequencies, Z = preprocessing.ignoreBelowX(frequencies, Z)

circuit.fit(frequencies, Z)
Z_fit = circuit.predict(frequencies)


## reciduales
res_meas_real = (Z - circuit.predict(frequencies)).real/np.abs(Z)
res_meas_imag = (Z - circuit.predict(frequencies)).imag/np.abs(Z)

# ploting 
#import matplotlib.pyplot as plt
#####fig, main_ax = plt.subplots()

#right_inset_ax = fig.add_axes([.25, .65, .4, .2], facecolor='w')

####plot_nyquist(main_ax, Z, fmt='o')
####plot_nyquist(main_ax, Z_fit, fmt='-')


fig, axs = plt.subplots(2,1)  # Create a figure and an axes.
import sys

ZmR = getMag(Z)
ZmT = getMag(Z_fit)

fase_R = getFase(Z)
fase_T = getFase(Z_fit)

#axs[0].plot(frequencies,fase_R,'*',label=' Real')
#axs[0].plot(frequencies,fase_T,  label='Teorico ')

#axs[1].plot(frequencies,ZmR,'*',label=' Real')
#axs[1].plot(frequencies,ZmT,label='Teorico ')

#seStyle(axs[0],n=2)
#seStyle(axs[1],n=3,eje2=True)

#axs[0].legend()

axs[1].plot(frequencies,np.real(Z),'*',label='  Real')
axs[1].plot(frequencies,np.real(Z_fit),  label='  Teorico ')

axs[0].plot(frequencies,-np.imag(Z),'*',label='  Real')
axs[0].plot(frequencies,-np.imag(Z_fit),  label='  Teorico ')

seStyle(axs[0],n=0)
seStyle(axs[1],n=1,eje2=True)
axs[0].legend()
#plot_residuals(right_inset_ax, frequencies, res_meas_real, res_meas_imag, y_limits=(-3,3))

#plt.plot(frequencies,Z)
plt.show()
#
print(circuit)
