import sys
from impedance import preprocessing
from impedance.models.circuits import CustomCircuit


import matplotlib.pyplot as plt
from impedance.visualization import plot_nyquist, plot_residuals
import numpy as np
#circuit = 'R0-p(R1,C1)-p(R2,C2)'
#initial_guess = [.01, .01, 100, .01, 0.001]


p = 0.000000000001

R0 = 20000
R1 = 100000
C0 =  100*p
    
circuit = 'R1-p(R2,C)'
#circuit = 'R0-p(R1,C1)-p(R2-L0,C2)'
initial_guess = [R0, R1, C0]
#initial_guess = [.01, .01, 100, .01, .05, 100, 1]



circuit = CustomCircuit(circuit, initial_guess=initial_guess)


# Load data from the example EIS result
frequencies, Z = preprocessing.readCSV('./datosImpedance.csv')
# keep only the impedance data in the first quandrant
frequencies, Z = preprocessing.ignoreBelowX(frequencies, Z)

circuit.fit(frequencies, Z)
Z_fit = circuit.predict(frequencies)


## reciduales
res_meas_real = (Z - circuit.predict(frequencies)).real/np.abs(Z)
res_meas_imag = (Z - circuit.predict(frequencies)).imag/np.abs(Z)

# ploting 
#import matplotlib.pyplot as plt
fig, main_ax = plt.subplots()

#right_inset_ax = fig.add_axes([.25, .65, .4, .2], facecolor='w')

plot_nyquist(main_ax, Z, fmt='o')
plot_nyquist(main_ax, Z_fit, fmt='-')

#plot_residuals(right_inset_ax, frequencies, res_meas_real, res_meas_imag, y_limits=(-3,3))

#plt.plot(frequencies,Z)
plt.show()
#
print(circuit)
