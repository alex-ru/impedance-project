from os import path
import os
from os import listdir
import datetime 
import numpy as np
import matplotlib.pyplot as plt
import math
import sys

def seStyle(ax):
    ax.tick_params(axis='both', which='major', labelsize=14)     

    ax.locator_params(axis='x', nbins=10, tight=True)
    ax.locator_params(axis='y', nbins=6, tight=True)


    #ax.legend(title="Concentración", loc='upper left')  # Add a legend.
    y_offset = ax.yaxis.get_offset_text()
    y_offset.set_size(18)
    t = ax.xaxis.get_offset_text()
    t.set_size(18)
    
    x_offset = ax.xaxis.get_offset_text()
    x_offset.set_size(18)
    tx = ax.xaxis.get_offset_text()
    tx.set_size(18)
    
    ax.grid(b=True, which='major', axis='both', alpha=.5)


def diferencia(v1,v2):
    v = []
    for i in range(len(v1)):
        v.extend([v1[i]- v2[i]])
    return v

def suma(v1,v2):
    v = []
    for i in range(len(v1)):
        v.extend([v1[i]+v2[i]])
    return v

def modelo():
    
    p = 0.000000000001

    R1 = 20000
    R2 = 68000
    C =  100*p
    Zr = []
    Zi = []
    Fr = []
    Z  = []
    fase = []
    
    for f in range(200000,9000,-1000):
        #WL = 1j*(2*math.pi*f*L)
        WC = -1j/(2*math.pi*f*C)
        z =  R1 + (WC*R2)/(WC+R2) #+ WL
        Z.extend([abs(z)])
        Fr.extend([f])
        Zr.extend([z.real])
        Zi.extend([-z.imag])
        fase.extend([math.degrees(math.atan2(-z.imag,z.real))])
        
    return [Fr,fase,Zr,Zi,Z]

def getXRXI(z,fase):
    xr = []
    xi = []
    for i in range(len(fase)):
        xr.extend([z[i]*math.cos(math.radians(fase[i]))])
        xi.extend([z[i]*math.sin(math.radians(fase[i]))])

    return [xr,xi]

def ordenar(lista):
    new_list = []
    for i in range(len(lista)):
            minimo = min(lista)
            new_list.extend([minimo])
            lista.remove(minimo)
            
    new_list = new_list
    return new_list
    
def getFileList(path):
    lista = []
    if os.path.isdir(path): # se la ruta existe 
            listfiles = listdir(path) # lista de archivos dentro de esa ruta
            
            for file in listfiles:
                if(("txt" in file)and("RC" in file)):
                    lista.extend([file])                
                    
    return lista

def getMePath():
    path = __file__
    path = path.split('\\')
    p=''
    for i in range(len(path)-1):
        p += (path[i]+'/')
    if(p==''):
        p =  "D:/alejandro/ProyectofGradute/mainProject/impedance-proyect/pruevas/calibracion/RC/"
        return p
    else:
        return(p)

def dataView(f,y,n=10):
    y1 = []
    f1 = []
    for i in range(n,len(y)-n):
        yd = 0
        for j in range(-n,n):
            yd = y[i-j] + yd
        yd = yd/((2*n)+1)
        y1.extend([yd])
        #x1.extend([(x[i-1] + x[i] + x[i+1])/3])
        #y1.extend([(y[i-2]+y[i-1] + y[i] + y[i+1] +y[i+2])/5])
        f1.extend([f[i]])
    return [f1,y1]

def exportTxt(m,filename):
        #ruta = "D:/alejandro/ProyectofGradute/mainProyect/impedance-project/pruevas/cafe_1/tendencia/"
        #filename = "cafe_{}.txt".format(n)
        fileSave = open (filename+'.txt','w')
        fileSave.write(" f(hz)      dfase(°)     dZ(ohm)  ")    
        fileSave.write('\n')
        for i in range(len(m[0])):
            for j in range(len(m)):
                dato = "{0:.11f}".format(m[j][i])
                if (m[j][i]>0):
                    fileSave.write(dato[0:8])
                else:
                    fileSave.write(dato[0:8])
                fileSave.write("   ")
            fileSave.write('\n')
        fileSave.close()

        print("funcion para exportar txt")
        
def loadTxt(pathfileload):

    f = open(pathfileload,"r")
    matriz = []
    readed =  False
    datap = f.readline()
    while(readed == False):
        data = f.readline()
        fil = data.split(" ")
        fil  = fil[0:len(fil)-1]
        filnew = [] 
        for d in fil:
            if(d!=''):
                #print(d)
                filnew.extend([float(d)])
        if(filnew==[]):
            readed = True
        if(filnew!=[]):
            matriz.extend([filnew])

    return matriz

path= getMePath()
lista = getFileList(path)

G = 10000
#M = loadTxt(path+'/'+"test_0.txt")
#[f0,fase0,amp0,ampr0] = np.transpose(M)


Z  = []
XR = []
XI = []
FASE = []

for j in range(len(lista)):
    sp = 0.1
    Z1=[]
    xr1=[]
    xi1=[]
    fase =[]
    
    M1 = loadTxt(path+'/'+lista[j])
    print(len(M1[0]))
    [f,fase,amp,ampr] = np.transpose(M1)
    #fase = fase - fase0
    spv = np.ones(len(f))*sp
    
    for i in range(len(fase)):
        z = ampr[i]/(amp[i]/G) # G =1000008
        Z1.extend([z])
        xr1.extend([z*math.cos(math.radians(fase[i]))+0 ]) # calibracion
        xi1.extend([z*math.sin(math.radians(fase[i]))])

    Z.extend([Z1])
    XR.extend([xr1])
    XI.extend([xi1])
    FASE.extend([fase])

fig, ax = plt.subplots()  # Create a figure and an axes.

[frecuencia,faseT,ZrT,ZiT,ZT] = modelo()

ZR    = Z[0]
ZrR   = XR[0]
ZiR   = XI[0]
faseR = FASE[0]

#ZTT = ZT
#faseTT = faseT
#[f1,faseT] = dataView(f,faseT)
#[f2,faseR] = dataView(f,faseR)

#[f3,ZT] = dataView(f,ZT,n=4)
#[f4,ZR] = dataView(f,ZR,n=4)

#print(len(f3))
#print(len(frecuencia))

#ax.plot(ZrR,ZiR,label=' Real')
#ax.plot(ZrT,ZiT,label=' Teorico')

dZ    = diferencia(ZT,ZR)
dfase    = diferencia(faseT,faseR)

exportTxt([f,dfase,dZ],'calibracionRC')
faseR = suma(faseR,dfase)
ZR = suma(ZR,dZ)

[xrNew,xiNew] = getXRXI(ZR,faseR)
[xrNewT,xiNewT] = getXRXI(ZT,faseT)

#ax.plot(xrNew,xiNew,label=' fase R')

#ax.plot(xrNewT,xiNewT,label=' fase T')
#ax.plot(ZrT,ZiT,label=' fase TO')

ax.plot(frecuencia,faseR,'*',label=' fase R')
ax.plot(frecuencia,faseT,label=' fase T')

#ax.plot(f3,ZR,label=' ZR')
#ax.plot(f4,ZT,label=' ZT')


ax.set_xlabel('frecuencia [Hz]')  # Add an x-label to the axes.
ax.set_ylabel('fase [ ° ]')  # Add a y-label to the axes.
ax.set_title('circuito RC:  R1 + p(R2,C1)')  # Add a title to the axes.
#ax.grid()
seStyle(ax)
#ax.set_title('cafe sin pulpa fermentando (20 litros) {}'.format(t0.date()))  # Add a title to the axes.

ax.legend()  # Add a legend.
plt.show()


