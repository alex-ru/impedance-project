from os import path
import datetime 
import sys
import numpy as np
import matplotlib.pyplot as plt


def seStyle(ax,eje2=False):

    units='Volt'
    unitsx = 'Hz'
    ax.tick_params(axis='both', which='major', labelsize=14)     

    ax.locator_params(axis='x', nbins=6, tight=True)
    ax.locator_params(axis='y', nbins=6, tight=True)

    if(eje2==True):
            ax.set_ylabel(r'$Error$ ' +
                  '[ % ]', fontsize=14)
    if(eje2==False):
        ax.set_ylabel(r'$Amplitud(\omega)$ ' +
                    '$[{}]$'.format(units), fontsize=14)
    
        ax.set_xlabel(r'$frecuencia $ ' +
                    '$[{}]$'.format(unitsx), fontsize=14)

    #ax.legend(title="Concentración", loc='upper left')  # Add a legend.
    y_offset = ax.yaxis.get_offset_text()
    y_offset.set_size(8)
    t = ax.xaxis.get_offset_text()
    t.set_size(18)
    ax.grid(b=True, which='major', axis='both', alpha=.5)
        
def loadTxt(pathfileload):

    f = open(pathfileload,"r")
    matriz = []
    readed =  False
    datap = f.readline()
    
    while(readed == False):
        data = f.readline()
        fil = data.split(" ")
        fil  = fil[0:len(fil)-1]
        filnew = [] 
        for d in fil:
            if(d!=''):
                #print(d)
                filnew.extend([float(d)])
        if(filnew==[]):
            readed = True
        if(filnew!=[]):
            matriz.extend([filnew])

    return matriz



F  = []
A1 = []
A2 = []
FASE = []

M1 = loadTxt("test_68K_0.txt")        
[f,fase,amp,ampr] = np.transpose(M1)
sp = 1
spv = np.ones(len(f))*sp

res = spv - (ampr + 0.06)
fig, ax = plt.subplots(2, 1, gridspec_kw={'height_ratios': [1, 2]})

ax[1].plot(f,ampr + 0.06,label=' Amplitud señal aplicada')
ax[1].plot(f,spv,label=' Amplitud deseada')
ax[1].set_ylim(0.5,1.5)
ax[0].set_title("Sistema con control ")

ax[0].plot(f,(res/sp)*100,'k')
ax[0].set_ylim(-10,10)
ax[0].grid()

seStyle(ax[0],True)
seStyle(ax[1])
ax[1].legend()
plt.show()
sys.exit()

 

