import matplotlib.pyplot as plt

fig, ax = plt.subplots(2, 1, gridspec_kw={'height_ratios': [1, 2]})

ax[0].plot(range(5), range(5, 10))
ax[1].plot(range(5), range(5, 10))

plt.show()
