from os import path
import datetime 
  

def dataView(f,y):
    #print(len(f))
    #print(len(y))
    #x1 = []
    y1 = []
    f1 = []
    for i in range(2,len(y)-2):
        #x1.extend([(x[i-1] + x[i] + x[i+1])/3])
        y1.extend([(y[i-2]+y[i-1] + y[i] + y[i+1] +y[i+2])/5])
        f1.extend([f[i]])
    return [f1,y1]
    

#return 0

def exportTxt(m,filename):
        #ruta = "D:/alejandro/ProyectofGradute/mainProyect/impedance-project/pruevas/calibracion/ten/"
        #filename = "cafe_{}.txt".format(n)
        fileSave = open (filename,'w')
        fileSave.write(" f(hz)      Fase(°)     VR(v)      VS(v)   ")    
        fileSave.write('\n')
        for i in range(len(m[0])):
            for j in range(len(m)):
                dato = "{0:.11f}".format(m[j][i])
                if (m[j][i]>0):
                    fileSave.write(dato[0:8])
                else:
                    fileSave.write(dato[0:8])
                fileSave.write("   ")
            fileSave.write('\n')
        fileSave.close()

        print("funcion para exportar txt")
        
def loadCorreTxt(pathfileload):

    f = open(pathfileload,"r")
    matriz = []
    readed =  False
    datap = f.readline()
    
    while(readed == False):
        data = f.readline()
        fil = data.split(" ")
        fil  = fil[0:len(fil)-1]
        filnew = [] 
        for d in fil:
            if(d!=''):
                #print(d)
                filnew.extend([float(d)])
        if(filnew==[]):
            readed = True
        if(filnew!=[]):
            matriz.extend([filnew])

    return matriz


import numpy as np
import matplotlib.pyplot as plt
import math


#pathfileload  = "D:/alejandro/ProyectofGradute/mainProyect/impedance-project/pruevas/aguaLimon_2/"

filename = "test_teC_{}.txt"
#filename1 = "cafe_1.txt"
#filename2 = "cafe_2.txt"

F  = []
A1 = []
A2 = []
FASE = []
for j in range(2):
    
    path1 = 'D:/alejandro/ProyectofGradute/mainProyect/impedance-project/pruevas/calibracion/capacitor/'
    if(path.exists(path1+filename.format(j))):
        M1 = loadCorreTxt(path1+filename.format(j))
        
        [f,fase,amp,ampr] = np.transpose(M1)
           
        A1.extend([ampr])
        A2.extend([amp])
        FASE.extend([fase])
        
        #print(len(f))
        #print(len(fase))


fig, ax = plt.subplots()  # Create a figure and an axes.

for i in range(1,len(FASE)):
    faseFinal = FASE[i]-FASE[0]
    ax.plot(f,FASE[i]-FASE[0],label='medicion {}'.format(i))

ax.set_xlabel('frecuencia [Hz]')  # Add an x-label to the axes.
ax.set_ylabel('fase [ ° ]')  # Add a y-label to the axes.
ax.set_title('cafe sin pulpa fermentando (20 litros)')  # Add a title to the axes.
ax.legend()  # Add a legend.


fig, ax = plt.subplots()  # Create a figure and an axes.

dV = A1[0]- A2[0]

sp = 1
spv = np.ones(len(f))*sp

for i in range(1,len(FASE)):
    AmpliFinal = A1[1]/A1[0]#A1[1]- A1[0] + A2[0]
    refFinal = A2[1]
    #ax.plot(f,A2[0] ,'k',label='medicion {}'.format(i))
    #ax.plot(f,A1[1]- A1[0] + A2[0] ,'b',label='medicion {}'.format(i))

Z  = []
xr = []
xi = []
print(len(AmpliFinal))
print(len(refFinal))
for i in range(len(faseFinal)):
    z = refFinal[i]/(AmpliFinal[i]/58000) # G =1000008
    Z.extend([z])
    xr.extend([z*math.cos(-math.radians(fase[i]))+0 ]) # calibracion
    xi.extend([-z*math.sin(-math.radians(fase[i]))])

#ax.plot(xr,'b',label='medicion {}'.format(i))
ax.plot(faseFinal,'b',label='medicion {}'.format(i))

ax.set_xlabel('ZR')  # Add an x-label to the axes.
ax.set_ylabel(' Zi')  # Add a y-label to the axes.
ax.set_title(' capacitor')  # Add a title to the axes.
ax.legend()  # Add a legend.


plt.show()


