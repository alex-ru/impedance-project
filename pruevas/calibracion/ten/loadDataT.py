from os import path
import datetime 

def seStyle(ax):
    ax.tick_params(axis='both', which='major', labelsize=14)     

    ax.locator_params(axis='x', nbins=10, tight=True)
    ax.locator_params(axis='y', nbins=6, tight=True)


    #ax.legend(title="Concentración", loc='upper left')  # Add a legend.
    y_offset = ax.yaxis.get_offset_text()
    y_offset.set_size(18)
    t = ax.xaxis.get_offset_text()
    t.set_size(18)
    
    x_offset = ax.xaxis.get_offset_text()
    x_offset.set_size(18)
    tx = ax.xaxis.get_offset_text()
    tx.set_size(18)
    
    ax.grid(b=True, which='major', axis='both', alpha=.5)


def dataView(f,y):
    #print(len(f))
    #print(len(y))
    #x1 = []
    y1 = []
    f1 = []
    for i in range(2,len(y)-2):
        #x1.extend([(x[i-1] + x[i] + x[i+1])/3])
        y1.extend([(y[i-2]+y[i-1] + y[i] + y[i+1] +y[i+2])/5])
        f1.extend([f[i]])
    return [f1,y1]
    

#return 0

def exportTxt(m,filename):
        ruta =  "D:/alejandro/ProyectofGradute/mainProject/impedance-proyect/pruevas/calibracion/ten/"
        #filename = "cafe_{}.txt".format(n)
        fileSave = open (ruta+filename,'w')
        fileSave.write(" f(hz)      Fase(°)     VR(v)      VS(v)   ")    
        fileSave.write('\n')
        for i in range(len(m[0])):
            for j in range(len(m)):
                dato = "{0:.11f}".format(m[j][i])
                if (m[j][i]>0):
                    fileSave.write(dato[0:8])
                else:
                    fileSave.write(dato[0:8])
                fileSave.write("   ")
            fileSave.write('\n')
        fileSave.close()

        print("funcion para exportar txt")
        
def loadCorreTxt(pathfileload):

    f = open(pathfileload,"r")
    matriz = []
    readed =  False
    datap = f.readline()
    
    while(readed == False):
        data = f.readline()
        fil = data.split(" ")
        fil  = fil[0:len(fil)-1]
        filnew = [] 
        for d in fil:
            if(d!=''):
                #print(d)
                filnew.extend([float(d)])
        if(filnew==[]):
            readed = True
        if(filnew!=[]):
            matriz.extend([filnew])

    return matriz


import numpy as np
import matplotlib.pyplot as plt
import math


#pathfileload  = "D:/alejandro/ProyectofGradute/mainProyect/impedance-project/pruevas/aguaLimon_2/"

filename = "test_ten_{}.txt"
#filename1 = "cafe_1.txt"
#filename2 = "cafe_2.txt"

F  = []
A1 = []
A2 = []
FASE = []

path1 = 'D:/alejandro/ProyectofGradute/mainProject/impedance-proyect/pruevas/calibracion/ten/'
 
for j in range(5):
 
    if(path.exists(path1+filename.format(j))):
        print(path1)
    
        M1 = loadCorreTxt(path1+filename.format(j))
        
        [f,fase,amp,ampr] = np.transpose(M1)
           
        A1.extend([ampr])
        A2.extend([amp])
        FASE.extend([fase])
        
        #print(len(f))
        #print(len(fase))


fig, axs = plt.subplots(2,1)  # Create a figure and an axes.

for i in range(1,len(FASE)):
    axs[0].plot(f,FASE[i]-FASE[0],label='medicion {}'.format(i))

axs[0].set_ylim(-0.7,4)
#axs[0].set_xlabel('frecuencia [Hz]')  # Add an x-label to the axes.
axs[0].set_ylabel('fase [ ° ]')  # Add a y-label to the axes.
axs[0].set_title('Señal de respuesta resistor 68K ')  # Add a title to the axes.
axs[0].legend()  # Add a legend.
 

seStyle(axs[0])
seStyle(axs[1])

dV = A1[0]- A2[0]

sp = 1
spv = np.ones(len(f))*sp

for i in range(1,len(FASE)):
    #ax.plot(f,A2[0] ,'k',label='medicion {}'.format(i))
    AReal = A1[i]- A1[0] + A2[0] +0.1  
    axs[1].plot(f,AReal ,label='medicion {}'.format(i))
    #axs[1].plot(f, ((AReal - 1)/1)*100  ,label='medicion {}'.format(i))

axs[1].set_ylim(0.7,1.2)
axs[1].set_xlabel('frecuencia [Hz]')  # Add an x-label to the axes.
axs[1].set_ylabel('AMPLITUD [ V ]')  # Add a y-label to the axes.
#axs[1].set_title('Señal de respuesta ( AMPLITUD ) resistor 68K ')  # Add a title to the axes.
axs[1].legend()  # Add a legend.
 

plt.show()


