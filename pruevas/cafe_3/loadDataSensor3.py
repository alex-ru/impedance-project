from os import path
import os
from os import listdir
import datetime 
import numpy as np
import matplotlib.pyplot as plt
import math
import sys 

def convert(datos):
    ph = []
    humedad = []
    tempcoffe = []
    tempAmb = []
    tiempo  = [] 
    
    for j in range(len(datos[0])):
        tempAmb.extend(  [float(datos[2][j])])
        tempcoffe.extend([float(datos[3][j])])
        ph.extend(       [float(datos[4][j])])
        humedad.extend(  [float(datos[5][j])])

        dato = datos[0][j] +' '+ datos[1][j]
        d1 = dato.split(".")[0]
        t1 = datetime.datetime.strptime(d1, '%Y-%m-%d %H:%M:%S')
        tiempo.extend([t1])
        
    return [ph,humedad,tempcoffe,tempAmb,tiempo]

def getFileList(path):
    if os.path.isdir(path): # se la ruta existe 
            listfiles = listdir(path) # lista de archivos dentro de esa ruta
            lista = []
            for file in listfiles:
                if(("txt" in file)and("cafe" in file)):
                    lista.extend([file])
                    print(file)
    return lista

def getMePath():
    path = __file__
    path = path.split('\\')
    p=''
    for i in range(len(path)-1):
        p += (path[i]+'/')
    # print(p)
    return(p)

def dataView(f,y,n=10):
    y1 = []
    f1 = []
    for i in range(n,len(y)-n):
        yd = 0
        for j in range(-n,n):
            yd = y[i-j] + yd
        yd = yd/((2*n)+1)
        y1.extend([yd])
        #x1.extend([(x[i-1] + x[i] + x[i+1])/3])
        #y1.extend([(y[i-2]+y[i-1] + y[i] + y[i+1] +y[i+2])/5])
        f1.extend([f[i]])
    return [f1,y1]

def exportTxt(m,filename):
        #ruta = "D:/alejandro/ProyectofGradute/mainProyect/impedance-project/pruevas/cafe_1/tendencia/"
        #filename = "cafe_{}.txt".format(n)
        fileSave = open (ruta+filename,'w')
        fileSave.write(" f(hz)      Fase(°)     XR(ohm)      XI(ohm)   ")    
        fileSave.write('\n')
        for i in range(len(m[0])):
            for j in range(len(m)):
                dato = "{0:.11f}".format(m[j][i])
                if (m[j][i]>0):
                    fileSave.write(dato[0:8])
                else:
                    fileSave.write(dato[0:8])
                fileSave.write("   ")
            fileSave.write('\n')
        fileSave.close()

        print("funcion para exportar txt")
        
def loadTxtSensor(pathfileload):

    f = open(pathfileload,"r")
    matriz = []
    readed =  False
    datap = f.readline()
    while(readed == False):
        data = f.readline()
        fil = data.split(" ")
        fil  = fil[0:len(fil)-1]
        filnew = [] 
        for d in fil:
            if(d!=''):
                #print(d)
                #if("2020" in d):
                    #filnew.extend([d])
                #if(":" in d):
                    filnew.extend([str(d)])
                #if((("2020" in d)==False)and((":" in d))==False):
                    #filnew.extend([float(d)])
    
        if(filnew==[]):
            readed = True
        if(filnew!=[]):
            matriz.extend([filnew])

    return matriz

path1 = getMePath()

M = loadTxtSensor(path1+"datosSensores3.txt")
M1 = np.transpose(M)

[ph,humedad,tempcoffe,tempAmb,tiempo] = convert(M1)

#plt.plot(tiempo,ph)
#plt.show()
#sys.exit()

fig, axs = plt.subplots(3, 1)

    
axs[0].plot(tiempo,ph,label='ph')
axs[1].plot(tiempo,humedad,label='humedad')
axs[2].plot(tiempo,tempAmb,label='T° ambiente')
axs[2].plot(tiempo,tempcoffe,label='T° cafe')

axs[0].set_ylim(2,7)
axs[1].set_ylim(50,100)
axs[2].set_ylim(15,32)

axs[2].set_xlabel('tiempo')  # Add an x-label to the axes.
axs[0].set_ylabel('pH')  # Add a y-label to the axes.
axs[1].set_ylabel('humedad [%]')  # Add a y-label to the axes.
axs[2].set_ylabel('Temperaturas [ °C ]')  # Add a y-label to the axes.
axs[0].set_title('cafe sin pulpa fermentando (1 Kg variedad colombia)')  # Add a title to the axes.
#ax.set_title('cafe sin pulpa fermentando (20 litros) {}'.format(t0.date()))  # Add a title to the axes.

axs[0].legend()  # Add a legend.
axs[1].legend()  # Add a legend.
axs[2].legend()  # Add a legend.

axs[0].grid()  # Add a legend.
axs[1].grid()  # Add a legend.
axs[2].grid()  # Add a legend.

plt.show()
