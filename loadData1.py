

import datetime 

def loadCorreTxt(pathfileload):

    f = open(pathfileload,"r")
    matriz = []
    readed =  False
    datap = f.readline()
    
    while(readed == False):
        data = f.readline()
        fil = data.split(" ")
        fil  = fil[0:len(fil)-1]
        filnew = [] 
        for d in fil:
            if(d!=''):
                #print(d)
                filnew.extend([float(d)])
        if(filnew==[]):
            readed = True
        if(filnew!=[]):
            matriz.extend([filnew])

    return matriz


import numpy as np
import matplotlib.pyplot as plt
import math


pathfileload  = "D:/alejandro/ProyectofGradute/mainProyect/impedance-project/pruevas/aguaLimon_2/"

filename = "cafe_{}.txt"
#filename1 = "cafe_1.txt"
#filename2 = "cafe_2.txt"

Z  = []
XR = []
XI = []
FASE = []
for j in range(30,48,2):
    sp = 0.1
    M1 = loadCorreTxt(pathfileload + filename.format(j))
    #M2 = loadCorreTxt(pathfileload2)
    [f,fase,amp,ampr] = np.transpose(M1)
    #[f1,fase1,amp1,ampr1] = np.transpose(M2)

    spv = np.ones(len(f))*sp

    Z1=[]
    xr1=[]
    xi1=[]
    for i in range(len(fase)):
        z = ampr[i]/(amp[i]/10000) # G =1000008
        Z1.extend([z])
        xr1.extend([z*math.cos(-math.radians(fase[i]))+0 ]) # calibracion
        xi1.extend([-z*math.sin(-math.radians(fase[i]))])

    Z.extend([Z1])
    XR.extend([xr1])
    XI.extend([xi1])
    FASE.extend([fase])
#plt.plot(f,fase)
#plt.plot(f,ampr)
#plt.plot(f,spv)
#plt.plot(f,amp)
#plt.plot(f,XR[0],'b')

fig, ax = plt.subplots()  # Create a figure and an axes.
#ax.plot(f,FASE[0],'b',label='0mm')
t0 = datetime.datetime(2019,9,6,7)
dt  = datetime.timedelta(minutes=30)
for j in range(len(XI)):
    i = j*2 + 30
    
    ax.plot(f[0:int(len(f)*0.5)],FASE[j][0:int(len(f)*0.5)],label=' {} '.format((t0+dt*j).time()))
#ax.plot(f,FASE[6],'g',label='time 2 ')
#ax.plot(f,FASE[3],'m',label='time 3 ')
#ax.plot(f,FASE[7],'r',label='time 4 ')
#ax.plot(f,FASE[8],'b',label='time 5 ')
#ax.plot(f,XI[4],'r',label='800mm^3')

ax.set_xlabel('frecuencia [Hz]')  # Add an x-label to the axes.
ax.set_ylabel('fase [ ° ]')  # Add a y-label to the axes.
ax.set_title('cafe sin pulpa fermentando (1Kg cerezas) {}'.format(t0.date()))  # Add a title to the axes.

ax.legend()  # Add a legend.
plt.show()


