import serial,time # interfaz arduino 
from scipy import signal
import numpy as np # manejo vectores 
import matplotlib.pyplot as plt # graficos
import math # matematicas
from scipy.fftpack import fft, ifft

from scipy.signal import butter, lfilter # filtros 
from scipy.signal import freqs # filtros 

import statistics as stats
from openpyxl import Workbook

def exportXLS(Datos=[],ns=5,name='datos'):
    print(name)
    wb = Workbook(write_only=True)
    ws = wb.create_sheet()
    M= np.zeros((len(Datos[0]),ns))
    rows = [['frecuencia','fase','fase1','amp','amp1']] #Frecuencia, Fase , Amplitud , Amplitud_Ref

    for row in rows:
        ws.append(row)
    
    for i in range(len(Datos[0])):
        for j in range(ns):
            M[i][j]=Datos[j][i]
            
    for irow in range(len(Datos[0])):
        ws.append(['%f' % M[irow][i]
                    for i in range(ns)])
    
    wb.save(name+'.xlsx')



def butter_Bandpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    cutoff1 = cutoff[0] / nyq
    cutoff2 = cutoff[1] / nyq
    b, a = butter(order, [cutoff1,cutoff2], btype='bandpass', analog=False)
    return b, a

def butter_Bandpass_filter(data, cutoff, fs, order=5):
    b, a = butter_Bandpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y

def mesureAmp(Signal):
    maxS = []
    minS = []
    for i in range(10):
        maxS.extend([max(Signal[int(0.1*i*len(Signal)):int(0.1*(i+1)*len(Signal))])])
        minS.extend([min(Signal[int(0.1*i*len(Signal)):int(0.1*(i+1)*len(Signal))])])
    maxR = sum(maxS)/len(maxS)
    minR = sum(minS)/len(minS)
    return (maxR-minR)*0.5

def eliminarOffset(Signal,Ref):
    
    maxS=[0,0,0,0,0,0,0,0,0,0]
    minS=[0,0,0,0,0,0,0,0,0,0]
    maxR=[0,0,0,0,0,0,0,0,0,0]
    minR=[0,0,0,0,0,0,0,0,0,0]

    for i in range(10):
        maxS[i] = max(Signal[int(0.1*i*len(Signal)):int(0.1*(i+1)*len(Signal))])
        minS[i] = min(Signal[int(0.1*i*len(Signal)):int(0.1*(i+1)*len(Signal))])

        maxR[i] = max(Ref[int(0.1*i*len(Ref)):int(0.1*(i+1)*len(Ref))])
        minR[i] = min(Ref[int(0.1*i*len(Ref)):int(0.1*(i+1)*len(Ref))])
    
    Vsignal = [0]
    Vref    = [0]

    minS1 = stats.mean(minS)  
    minR1 = stats.mean(minR)
    
    maxS1 = stats.mean(maxS) 
    maxR1 = stats.mean(maxR) 

    offsetS = (maxS1+minS1)*0.5
    offsetR = (maxR1+minR1)*0.5

    for i in range(len(Signal)):
        Vsignal.append(Signal[i])
        Vref.append(Ref[i])

    for i in range(len(Signal)):
        
        if(Vsignal[i]!=None):
            Vsignal[i] = Vsignal[i] - offsetS    
        if(Vref[i]!=None):
            Vref[i] = Vref[i] - offsetR    
        
        
    return [Vsignal,Vref]

def prome(v1):
    p = sum(v1)/len(v1)
    return p

def mixer(v1,v2):
    v = np.zeros(len(v1))
    for i in range(len(v1)):
        v[i]=v1[i]*v2[i]
    return v

def mixerM(v1,v2,v3,v4):
    M = np.zeros((2,len(v1)))
    for i in range(len(v1)):
        M[0][i]=v1[i]*v2[i]
        M[1][i]=v3[i]*v4[i]
    return M

def mesureFrecuency(Vref,f,fs):
    df = f*0.1
    Vref    = butter_Bandpass_filter(Vref, [f-df,f+df], fs, 2)
    
    ni = int(len(Vref)*0.5)
    nf = int(len(Vref))
    
    Vref = Vref[ni:nf]
    magnitud  = len(Vref)
    k=0
    zero=[0,0]
    for i in range(magnitud-1):
        if((Vref[i+1]>0 and Vref[i]<0) or(Vref[i+1]<0 and Vref[i]>0)) :
            
            if (abs(Vref[i+1])>abs(Vref[i]))and(k<2):
                 zero[k]=i
                 k=k+1
            if (abs(Vref[i+1])<abs(Vref[i]))and(k<2):
                zero[k]=i+1
                k=k+1
    
    T = 2*(zero[1]-zero[0])*(1/fs)
    return [f,1/T]
    
def interpolation(Signal,fs,h): #(muestras,frecuencia de muestreo,periodo de muestreo final)
    #h = 0.01 # periodo de muestreo recuperado (continuo) "Resolucion Importante"
    Nmuestras = len(Signal) #  numero de muestras discretas
    t =  np.arange(0,Nmuestras/fs,h) # vector de tiempo "continuo"
    SignalRecuperada =0
    for k in range(0,Nmuestras): 
      SignalRecuperada += Signal[k]*np.sinc( -k + fs * t)
      
    return [SignalRecuperada,t]

def interpolationM(Signal,Vref,fs,h): #(muestras,frecuencia de muestreo,periodo de muestreo final)
    #h = 0.01 # periodo de muestreo recuperado (continuo) "Resolucion Importante"
    Nmuestras = len(Signal) #  numero de muestras discretas
    t =  np.arange(0,Nmuestras/fs,h) # vector de tiempo "continuo"
    SignalRecuperada =0
    VrefRecuperada =0
    for k in range(0,Nmuestras): 
      SignalRecuperada += Signal[k]*np.sinc( -k + fs * t)
      VrefRecuperada += Vref[k]*np.sinc( -k + fs * t)
      
    return [SignalRecuperada,VrefRecuperada,t]

def pll(t1,Vs,Vref):
    magnitud  = len(Vs)
    k=0
    zero=[0,0]
    for i in range(magnitud-1):
        if((Vref[i+1]>0 and Vref[i]<0) or(Vref[i+1]<0 and Vref[i]>0)) :
            
            if (abs(Vref[i+1])>abs(Vref[i]))and(k<2):
                 zero[k]=i
                 k=k+1
            if (abs(Vref[i+1])<abs(Vref[i]))and(k<2):
                zero[k]=i+1
                k=k+1
    
    max1 = int((zero[1]+zero[0])/2)
    t = t1[max1:]
    Vs = Vs[zero[0]: magnitud - (max1-zero[0])]
    Vr1 = Vref[zero[0]: magnitud - (max1-zero[0]) ]
    Vr2 = Vref[max1:magnitud]
    
    return [t,Vs,Vr1,Vr2]

def detectarPico(v):
    pico= 0
    i=0
    while(i < len(v)):
        if(v[i]<v[i+1]):
            pico = i
            i=len(v)
        i=i+1
    return pico


def desfase(v):
    for i in range(len(v)):
        v[i] = -v[i]
    return v

def ver(Vsignal,Vref_1,Vref_2):
    plt.plot(Vsignal,'g')
    plt.plot(Vref_1,'b')
    plt.plot(Vref_2,'y')
    plt.grid()
    plt.show()

def lockin(Signal,Vr,fs,f):

    #plt.plot(Signal,'y')
    #plt.plot(Vr,'g')
    #plt.show()
    df = f*0.1
    
    Signal = butter_Bandpass_filter(Signal, [f-df,f+df],fs,2)
    Vr    = butter_Bandpass_filter(Vr, [f-df,f+df], fs, 2)

    t =  np.arange(0,(len(Vr)*(1/fs)),1/fs) # vector de tiempo "continuo"
  
    #Signal,Vr = eliminarOffset(Signal,Vr)

    #plt.plot(Signal,'r')
    #plt.plot(Vr,'b')
    #plt.show()
    
    #retardo1 = 35000#int(0.01*len(Vref_f))
    #retardo2 = 50000
    FM = 2*fs# frecuencia de muestreo de nitquiz
    #H = 1/FM # periodo de muestreo de nitquiz
    
    #M1 = interpolation(Signal[retardo1:retardo2],fs,1/FM)
    #M2 = interpolation(Vr[retardo1:retardo2],fs,1/FM)
    
    #[Vsignal_i,t_i]=interpolation(Signal[retardo1:retardo2],fs,1/FM)
    #[Vref_i,t_i]=interpolation(Vr[retardo1:retardo2],fs,1/FM)

    Vsignal_i = Signal
    Vref_i    = Vr
    t_i       = t
    
    
    #Vsignal_i = M1[0]
    #Vref_i    = M2[0]
    #t_i       = M1[1]


    #Vsignal_i = desfase(Vsignal_i)
    #plt.figure(2)
    #plt.plot(Vsignal_i)
    #plt.plot(Vref_i)
    #plt.grid()
    #plt.show()
    
    retardo = 20000
    #M = pll(t[retardo:],Signal[retardo:],Vr[retardo:])# PLL(tiemp1o,señal,referencia) 
    M = pll(t_i[retardo:],Vsignal_i[retardo:],Vref_i[retardo:])# PLL(tiemp1o,señal,referencia) 

    t       =     M[0]
    Vsignal  =    M[1]
    Vref_1   =    M[2]
    Vref_2   =    M[3]
    
    #plt.plot(Vsignal,'g')
    #plt.plot(Vref_1,'b')
    #plt.plot(Vref_2,'y')
    #plt.grid()
    #plt.show()
    
    #ver (Vsignal,Vref_1,Vref_2)

    #Aref1 = np.max(Vref_1[int(0.1*len(Vref_1)):int(0.3*len(Vref_1))]) # amplitud señal de referencia
    #Aref2 = np.max(Vref_1[int(0.3*len(Vref_1)):int(0.6*len(Vref_1))]) # amplitud señal de referencia
    #Aref3 = np.max(Vref_1[int(0.6*len(Vref_1)):int(0.9*len(Vref_1))]) # amplitud señal de referencia
    #Aref4 = np.max(Vref_1[int(0.4*len(Vref_1)):int(0.5*len(Vref_1))]) # amplitud señal de referencia
    #Aref5 = np.max(Vref_1[int(0.5*len(Vref_1)):int(0.6*len(Vref_1))]) # amplitud señal de referencia
    #Aref = prome([Aref1,Aref2,Aref3,Aref4,Aref5])
    #Aref = prome([Aref1,Aref2,Aref3])

    Aref = mesureAmp(Vref_1)
    Amplitud = mesureAmp(Vsignal)
 
    
    VD1 = mixer(Vsignal,Vref_1)  
    VD2 = mixer(Vsignal,Vref_2) 

    VCD1 = VD1
    VCD2 = VD2
    
    value1 = int(len(VCD1)*0.2)
    value2 = int(len(VCD1)*0.7)
    
    X = prome(VCD1[value1 : value2])
    Y = prome(VCD2[value1 : value2])
    
    A1   =  math.sqrt((X**2)+(Y**2))
    
    Fase =  math.atan2(Y,X)
    Fase  = math.degrees(Fase)
    
    #if(Y<0 and X<0):
        #Fase = Fase + 360 
        
    #if(Y<0 and X>0):
        #Fase = Fase + 360     
        
    ####Amplitud  = ((2*A1)/Aref)  ### error de calculo :(  
    
    dA=0#-0.47078237952800706
    dF= 0#+ 3.7698654136760332
    Fase = Fase +dF
    Amplitud = Amplitud + dA
    

    if(Y<0):
        if(abs(Fase)<2):
            Fase = abs(Fase)
        else:
            Fase = Fase + 360

    if(Fase >200):
        Fase = abs(Fase -360)

    #print("Amplitud  :" +str(Amplitud))
    #print("fase  :" +str(Fase))
    
    return[Amplitud,Aref,Fase]
 
