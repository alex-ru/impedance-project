
import serial
import matplotlib.pyplot as plt 
from datetime import datetime, timedelta
import mesureAuto

def limpiarChar(s,ruido):
    DatoLimpio = ""
    for letra in s:
        if letra not in ruido:
            DatoLimpio += letra
    return DatoLimpio


def exportDataSensor(m,filename="datosSensores_8.txt"):
            ruta = "D:/alejandro/ProyectofGradute/mainProyect/impedance-project/pruevas/cafe_8/"
            #filename = "cafe_{}.txt".format(n)
            fileSave = open (ruta+filename,'w')
            fileSave.write("     tiempo                   tempAm     tempCafe    ph     humedad ")    
            fileSave.write('\n')
            for i in range(len(m[0])):
                for j in range(len(m)):
                    if (type(m[j][i]) != float):
                        dato = m[j][i]
                        fileSave.write(str(dato))
                    else:
                        dato = "{0:.11f}".format(m[j][i])
                        if (m[j][i]):
                            fileSave.write(dato[0:8])
                        else:
                            fileSave.write(dato[0:8])
                    fileSave.write("   ")
                fileSave.write('\n')
            fileSave.close()

            print("funcion para exportar txt de los sensores ")
            

class MesureSensors():
    

        
    
    def drawData(self):
        self.axs[0].clear() # funciona 
        self.axs[1].clear() # funciona 

        self.axs[0].plot(self.tiempo,self.ph,'k',label='ph')
        

        self.axs[1].plot(self.tiempo,self.tempAmbiente,'g',label=' tem ambiente')
        #self.axs[1].plot(self.tiempo,self.humedad,'m',label=' humedad')
        self.axs[1].plot(self.tiempo,self.tempCafe,'b',label='tem cafe ')
        #i = self.i
        #self.axs[0].plot(i,i,'*r')
        self.axs[0].set_xlabel('n')
        self.axs[0].set_ylabel(' ')

        self.axs[1].set_xlabel('')
        self.axs[1].set_ylabel('')

        self.axs[0].legend()  # Add a legend.
        self.axs[1].legend()  # Add a legend.
        return 0
    
    def __init__(self,filename="cafe_0.txt"):

        now1 = datetime.now()
        h1 = now1.hour
        m1 = now1.minute
        #print(now1)
        filename1="datosSensores_{}_{}.txt".format(h1,m1)
            
        serialport = serial.Serial('COM4',baudrate=9600,timeout=0)
        serialport.close()
        serialport.open()
        serialport.close()
        datos = ''

        timeInitTotal  = datetime(2020,10,21,19,48)
        timeEndTotal   = datetime(2020,10,22,16,0)
        
        self.tempCafe = []
        self.tempAmbiente = []
        self.humedad = []
        self.ph = []
        self.tiempo = []
        
        self.filename = filename
        self.i = 0      

        plt.ion()    
        self.fig, self.axs = plt.subplots(2,figsize=(10, 5)) #, constrained_layout=True)
        self.axs[0].grid()
        self.axs[1].grid()
        #plt.title("Medicion de variables ")
        plt.show()
        mesured = False
        i = 0 
        lastMesureI = timeInitTotal
        lastMesureS = timeInitTotal
        while(mesured == False):
            
            now = datetime.now()

            if (now > timeEndTotal):
                mesured = True
                exportDataSensor([self.tiempo,self.tempAmbiente,self.tempCafe,self.ph,self.humedad],filename1)
            #print(now)
            #print(lastMesureI)
            #print((now-lastMesureI).total_seconds())
            
            if((now-lastMesureI).total_seconds() > 3600):
                lastMesureI = now
                #IMP = mesureAuto.MesureImpedance('cafe_'+ str(now.hour)+str(now.minute)+'_'+str(i)+'.txt')
                #del IMP
                print("prende el equipo ya ")
                i = i + 1

            if((now-lastMesureS).total_seconds() > 600):
                serialport.open()
        
                testsensor = False
                lastMesureS = now
                while(testsensor==False):
                    
                    if(serialport.in_waiting > 0):
                        
                        d = serialport.read(1)
                        datos = datos + str(d)

                        #if(len(self.ph)>5):
                            #print("medicion terminada.  jajaj era mentiras")
                            #exportDataSensor([self.tiempo,self.tempAmbiente,self.tempCafe,self.ph,self.humedad])
                            #self.exportTxt(self.filename)
                            #mesured = True
                        
                        if('a' in str(d)):
                            datos = limpiarChar(datos,"b'\\nra")
                            temC = datos.split(',')[0]
                            temA = datos.split(',')[1]
                            h = datos.split(',')[2]
                            p = datos.split(',')[3]
                            now = datetime.now()
                            
                            self.tempCafe.extend([float(temC)])
                            self.tempAmbiente.extend([float(temA)])
                            self.humedad.extend([float(h)])
                            self.ph.extend([float(p)])
                            self.tiempo.extend([now])
                            testsensor = True
                            print(str(datos) + " : " + str(now))
                            exportDataSensor([self.tiempo,self.tempAmbiente,self.tempCafe,self.ph,self.humedad])
                            #print(now)
                            datos = ''
                            serialport.close()
                            self.drawData()
            
            plt.pause(0.1)

MesureSensors()
