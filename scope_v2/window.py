
import readScope 
import lockin
#  
import serial
from PyQt5.QtCore import QThread

#  traza
from matplotlib.backends.qt_compat import QtCore, QtWidgets, is_pyqt5
if is_pyqt5():
    from matplotlib.backends.backend_qt5agg import (
        FigureCanvas, NavigationToolbar2QT as NavigationToolbar)
else:
    from matplotlib.backends.backend_qt4agg import (
        FigureCanvas, NavigationToolbar2QT as NavigationToolbar)
from matplotlib.figure import Figure

import numpy as np
import time
#  traza

from PyQt5 import QtCore, QtGui, QtWidgets

class Arduino():
    def __init__(self,serialport=None):
        self.serialport = serialport

    def setPort(self,serialport):
        self.serialport = serialport

    def remove(self):
        self.serialport = None

class Data():
    def __init__(self,signal = None,ref=None,fs=None,frequency=100,voltaje=1,pasos=1,frequencyinit=0,frequencyend=200,df=1000):
        self.signal = signal
        self.ref = ref
        self.frequency = frequency
        self.voltaje = voltaje
        self.pasos = pasos 
        self.fs = fs
        self.df = df
        self.__frequencyinit = frequencyinit
        self.__frequencyend = frequencyend

    def getAmpPhase(self):
        [amp,ampr,phase] = lockin.lockin(self.signal,self.ref,self.fs,self.frequency)
        return [amp,ampr,phase]

    def getAmp(self):
        amp1 = lockin.mesureAmp(self.signal)
        amp2 = lockin.mesureAmp(self.ref)
        return amp2

class Impedancedata():
    def __init__(self,frequency=[],ampl=[],amplr=[],phase=[],xr=[],xi=[],z=[]):
        self.frequency = frequency
        self.ampl = ampl
        self.amplr = amplr 
        self.phase = phase
        self.xr = xr
        self.xi = xi
        self.z = z

    def addData(self,f,A,Ar,p):
        self.frequency.extend([f])
        self.ampl.extend([A])
        self.amplr.extend([Ar])
        self.phase.extend([p])

    def addFrequency(self,f):
        self.frequency.extend([f])

    def addAmpl(self,A):
        self.ampl.extend([A])

    def addPhase(self,p):
        self.phase.extend([p])
    
    def dataExp(self):
        f = self.frequency
        p = self.phase
        a  = self.ampl
        ar = self.amplr
        l = list()
        for i in range(len(p)):
            l.append((f[i],p[i],a[i],ar[i]))
        return l


    def processar(self):
        import math
        self.xr = []
        self.xi = []
        self.z = [] 
        Iv = self.ampl
        fase = self.phase
        Vr  = self.amplr
        for i in range(len(Vr)):
            z = Vr[i]/(Iv[i]/58000) # G =1000000
            self.z.extend([z])
            self.xr.extend([z*math.cos(-math.radians(fase[i]))+0 ]) # calibracion
            self.xi.extend([-z*math.sin(-math.radians(fase[i]))])

data1 = Data()
impedance = Impedancedata()
arduino1 = Arduino()

def getPasos(volts):
    #Volt = 0.0333*paso + 0.1374 relacion por minimos cuadrados
    pasos = int((volts - 0.1374)/0.0333)
    if(pasos<1):
        pasos = 1    #
    if(pasos>70):
        pasos = 70
    return pasos

def setVoltajeArduino(voltaje=None):
    data1.pasos = getPasos(voltaje)
    print("pasos es :"+ str(data1.pasos))
    #arduino = serial.Serial('COM9',baudrate=9600,timeout=0)
    time.sleep(2) #  como eliminarlo 
    dato =  str(data1.pasos)+"d"
    #print(dato)
    bandera = 0
    arduino1.serialport.write(dato.encode())
    dato=''
    response= ''
    while(bandera<100):
        if(arduino1.serialport.in_waiting > 0):
            response = arduino1.serialport.readline()
            #print(response)
        if(('y' in str(response))== True):
            bandera = 101
            #print("Voltaje ajustado" + str(voltaje))  
        bandera = bandera +1
    if(bandera==100):
        u=0
        #print("envio a arduino fallo")
    ##arduino.close()
    ##del arduino
    

def setFrequencyArduino(frequency,pasosArd):
    #arduino = serial.Serial('COM9',baudrate=9600,timeout=0)
    time.sleep(0.5)
    datos5 = str(pasosArd) +"g" + str(frequency)+"z"
    arduino1.serialport.write(datos5.encode()) # se envia la frecuencia con la que se relizara la medicion 
    #arduino.write(b'z')
    #arduino.close()
    #del arduino
    print("frecuencia asignada " + str(frequency))

def finishedMesureArduino():
    #arduino = serial.Serial('COM9',baudrate=9600,timeout=0)
    #time.sleep(2)
    arduino1.serialport.write(b'E')
    arduino1.serialport.close()
    arduino1.remove()
    #del arduino

def plusFrequency(mode='linal',f=0):
    #df = 100
    f = f + data1.df
    return f

def testConnectArduino():
    try:
        arduino = serial.Serial("COM9", 9600, timeout=0)
        while arduino.read():
            print('serial open')
        print('serial closed')
        arduino.close()
        return True
    except:
        print('Equipo desconectado')
        return False
    
def control():
    #print("se ejecuta el control ")
    frecuenciaMuestra = data1.frequency
    pasosMuentra  = data1.pasos
    setFrequencyArduino(frecuenciaMuestra,pasosMuentra)
    error = 1
    cont1 = 0
    cont2 = 0
    while((error>0.06)or(error<-0.06)):
        [ref,signal,fs] = readScope.getDataplus(frequency=frecuenciaMuestra)     
        
        #data1.signal = signal2
        #data1.ref = signal1 
        #data1.fs = fs      

        voltReal =   lockin.mesureAmp(ref)
        #voltReal = data1.getAmp()
        
        voltSP =   data1.voltaje

        error = voltReal - voltSP
        print("error : " + str(error))
        print("pasos : "+str(data1.pasos))
        #if((error>0.0333)or(error<-0.0333)):#0.06
        #if((error>0.06)or(error<-0.06)):#0.06
        dpasos = 1
        #dpasos = int((error)/0.0333)
        if(error>0):
            data1.pasos = data1.pasos - dpasos
        if(error<0):
            data1.pasos = data1.pasos + dpasos

        if (data1.pasos<1):
            data1.pasos = 1
            cont1 = cont1 + 1
        if(data1.pasos>90):
            data1.pasos = 90   
            cont2 = cont2 + 1
        pasosMuentra  = data1.pasos 
        setFrequencyArduino(frecuenciaMuestra,pasosMuentra)
        if ((cont1 ==3)or(cont2==3)):
            pasosMuentra  = data1.pasos 
            setFrequencyArduino(frecuenciaMuestra,pasosMuentra)
            return 0

    return 0 


class ReadingHantek(QThread):
    def __init__(self):
        super().__init__()
           
    def run(self):
        print("leyendo oscilocopio en segundo plano")
        frecuenciaMuestra = data1.frequency
        pasosMuentra = data1.pasos
        setFrequencyArduino(frecuenciaMuestra,pasosMuentra)
        [signal1,signal2,fs] = readScope.getDataplus(frequency=frecuenciaMuestra)     
        data1.signal = signal2
        data1.ref = signal1 
        data1.fs = fs      
        [amp,ampr,phase]=data1.getAmpPhase()
        impedance.addData(f=frecuenciaMuestra,A=amp,Ar=ampr,p=phase)
        print("Datos leidos y procesados : "+ str(frecuenciaMuestra)+'Hz')

class ReadingHantek1(QThread):
    def __init__(self):
        super().__init__()
           
    def run(self):
        control()
        #print("se ejecuta el control ")
        #frecuenciaMuestra = data1.frequency
        #pasosMuentra  = data1.pasos
        #setFrequencyArduino(frecuenciaMuestra,pasosMuentra)
        #[signal1,signal2,fs] = readScope.getDataplus(frequency=frecuenciaMuestra)     
        #data1.signal = signal2
        #data1.ref = signal1 
        #data1.fs = fs      

        #voltReal = data1.getAmp()
        #voltSP =   data1.voltaje
        #error = voltReal - voltSP
        #print("error 1 : " + str(error))
        #if((error>0.0333)or(error<-0.0333)):#0.06
        #if((error>0.06)or(error<-0.06)):#0.06
        #    dpasos = int((error)/0.0333)
        #    data1.pasos = data1.pasos - dpasos
        #if (data1.pasos<1):
        #    data1.pasos = 1
        #if(data1.pasos>70):
        #    data1.pasos = 70   
        pasosMuentra  = data1.pasos 
        #setFrequencyArduino(frecuenciaMuestra,pasosMuentra)
        
        #impedance.addData(f=frecuenciaMuestra,A=amp,Ar=ampr,p=phase)

        ## se ejecuta la medicion 
        frecuenciaMuestra =  data1.frequency
        [signal1,signal2,fs] = readScope.getDataplus(frequency=frecuenciaMuestra)     
        data1.signal = signal2
        data1.ref = signal1 
        data1.fs = fs      
        [amp,ampr,phase]=data1.getAmpPhase()
        impedance.addData(f=frecuenciaMuestra,A=amp,Ar=ampr,p=phase)
        #print("error 2 : " + str(amp-ampr))
        #print("pasos " + str(pasosMuentra))
        print("Datos leidos y procesados : "+ str(frecuenciaMuestra)+'Hz')

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setSizeConstraint(QtWidgets.QLayout.SetFixedSize)
        self.gridLayout.setObjectName("gridLayout")
        self.botonconfig = QtWidgets.QPushButton(self.centralwidget)
        self.botonconfig.setObjectName("botonconfig")
        self.gridLayout.addWidget(self.botonconfig, 5, 2, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem, 1, 2, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.label_4.setFont(font)
        self.label_4.setAlignment(QtCore.Qt.AlignCenter)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 0, 0, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem1, 7, 2, 1, 1)
        spacerItem2 = QtWidgets.QSpacerItem(58, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem2, 3, 1, 2, 1)

        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_3.addWidget(self.label_2)

        self.doubleSpinBox_frecuencia_f = QtWidgets.QDoubleSpinBox(self.centralwidget)
        self.doubleSpinBox_frecuencia_f.setMaximum(1000000.0)
        self.doubleSpinBox_frecuencia_f.setObjectName("doubleSpinBox_frecuencia_f")
        self.horizontalLayout_3.addWidget(self.doubleSpinBox_frecuencia_f)

        ############################################
        self.horizontalLayout_df = QtWidgets.QHBoxLayout()
        self.horizontalLayout_df.setObjectName("horizontalLayout_df")
        self.label_df = QtWidgets.QLabel(self.centralwidget)
        self.label_df.setObjectName("label_df")
        self.horizontalLayout_df.addWidget(self.label_df)
        self.doubleSpinBox_df = QtWidgets.QDoubleSpinBox(self.centralwidget)
        self.doubleSpinBox_df.setMaximum(5.0)
        self.doubleSpinBox_df.setObjectName("doubleSpinBox_df")
        self.horizontalLayout_df.addWidget(self.doubleSpinBox_df)

        self.gridLayout.addLayout(self.horizontalLayout_df, 7, 5, 1, 1)
        ######################################################

        self.gridLayout.addLayout(self.horizontalLayout_3, 3, 2, 1, 1)
        self.widget = QtWidgets.QWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.widget.sizePolicy().hasHeightForWidth())
        self.widget.setSizePolicy(sizePolicy)
        self.widget.setObjectName("widget")
        self.gridLayout.addWidget(self.widget, 1, 0, 7, 1)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.doubleSpinBox_frecuencia_i = QtWidgets.QDoubleSpinBox(self.centralwidget)
        self.doubleSpinBox_frecuencia_i.setMaximum(1000000.0)
        self.doubleSpinBox_frecuencia_i.setObjectName("doubleSpinBox_frecuencia_i")
        self.horizontalLayout.addWidget(self.doubleSpinBox_frecuencia_i)
        self.gridLayout.addLayout(self.horizontalLayout, 2, 2, 1, 1)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_2.addWidget(self.label_3)
        self.doubleSpinBox_voltaje = QtWidgets.QDoubleSpinBox(self.centralwidget)
        self.doubleSpinBox_voltaje.setDecimals(1)
        self.doubleSpinBox_voltaje.setMaximum(5.0)
        self.doubleSpinBox_voltaje.setObjectName("doubleSpinBox_voltaje")
        self.horizontalLayout_2.addWidget(self.doubleSpinBox_voltaje)
        self.gridLayout.addLayout(self.horizontalLayout_2, 4, 2, 1, 1)
        self.botonStart = QtWidgets.QPushButton(self.centralwidget)
        self.botonStart.setObjectName("botonStart")
        self.gridLayout.addWidget(self.botonStart, 6, 2, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 20))
        self.menubar.setObjectName("menubar")
        self.menuArchivo = QtWidgets.QMenu(self.menubar)
        self.menuArchivo.setObjectName("menuArchivo")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionGuardar = QtWidgets.QAction(MainWindow)
        self.actionGuardar.setObjectName("actionGuardar")
        self.actionExportar_txt = QtWidgets.QAction(MainWindow)
        self.actionExportar_txt.setObjectName("actionExportar_txt")
        self.menuArchivo.addAction(self.actionGuardar)
        self.menuArchivo.addAction(self.actionExportar_txt)
        self.menubar.addAction(self.menuArchivo.menuAction())
        
        self.gridLayouttraza = QtWidgets.QGridLayout(self.widget)
        traza = FigureCanvas(Figure(figsize=(5, 3)))
        self.gridLayouttraza.addWidget(traza)
        self.traza_ax = traza.figure.subplots()

        self.actionExportar_txt.triggered.connect(self.exportTxt)
        self.actionGuardar.triggered.connect(self.exportCVS)

        self.botonconfig.clicked.connect(self.config)
        self.botonStart.clicked.connect(self.start)
        
        self.t= -20 # variable dinamica para probar grafica
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
    
    def drawtraza(self):
        # pitar datos de impedancia
        #self.traza_ax.clear()
        #self.t = self.t + 1 
        #t = self.t
        #y = 20 - t*t
        #print(data1._Data__frequencyinit)
        #self.traza_ax.plot(t,y,'*r')
        self.traza_ax.clear()
        #self.traza_ax.plot(impedance.frequency,impedance.amplr,'-r')
        #self.traza_ax.plot(impedance.frequency,impedance.ampl,'-k')
        #self.traza_ax.plot(impedance.frequency,impedance.phase,'*k')
        self.traza_ax.plot(impedance.frequency,impedance.z,'*k')
        #self.traza_ax.plot(impedance.xr,impedance.xi,'*k')
        self.traza_ax.set_xlabel('Zr')
        self.traza_ax.set_ylabel('-Zi')
        self.traza_ax.grid()
        self.traza_ax.figure.canvas.draw()
    
    def drawPoint(self,x,y):
        
        self.traza_ax.plot(x,y,'*k')
        #self.traza_ax.grid()
        self.traza_ax.figure.canvas.draw()

    def config(self):
        data1.voltaje = 0.2

        if(testConnectArduino()==True):
            print("conectado")
            arduino1.setPort(serialport=serial.Serial('COM9',baudrate=9600,timeout=0))
            setVoltajeArduino(data1.voltaje)
            #data3 = list()
            #for i in range(70):
                
                #setVoltajeArduino(i+1)
                #time.sleep(2)
                #[signal1,signal2,fs] = readScope.getDataplus(frequency=10000)
                #m1 = max(signal1[10000:20000])
                #m2 = min(signal1[10000:20000])
                #A = (m1 - m2)*0.5
                #data3.append((i+1,A))
                #self.drawPoint(i+1,A)
                #print(str(i+1) + " " + str(A))
            #self.exportCVS(data3)
    def start(self):
        # leer datos de la interfaz        
        
        fi=200000
        ff=10000
        data1.df=-1000
        data1._Data__frequencyinit = fi
        data1._Data__frequencyend  = ff
        data1.frequency  = fi
        #setFrequencyArduino(20000)
        self.startMesure()

    def readFinished(self):
        impedance.processar()
        self.drawtraza()
        del self.reading
        if(data1.frequency<=data1._Data__frequencyend):
            finishedMesureArduino()
            print("Barrido de frecuencias terminado")
            self.exportTxt()
            #self.exportCVS(impedance.dataExp())
            return
        if(data1.frequency>data1._Data__frequencyend):
            data1.frequency = plusFrequency(mode='lineal',f=data1.frequency)
            self.startMesure()

    def startMesure(self):
        self.reading = ReadingHantek1() # 
        self.reading.finished.connect(self.readFinished) # lo que pasa cuando termina de leer
        self.reading.start()


    def exportTxt(self):
        ruta = "D:/alejandro\ProyectofGradute/mainProyect/impedance-project/pruevas/cafe_1/"
        filename = "cafe_21.txt"
        fileSave = open (ruta+filename,'w')
        m = impedance.dataExp()
        fileSave.write(" f(hz)      Fase(°)     Vs(v)      Vr(v)")    
        fileSave.write('\n')
        for i in range(len(m)):
            for j in range(len(m[0])):
                dato = "{0:.11f}".format(m[i][j])
                if (m[i][j]>0):
                    fileSave.write(dato[0:8])
                else:
                    fileSave.write(dato[0:8])
                fileSave.write("   ")
            fileSave.write('\n')
        fileSave.close()

        print("funcion para exportar txt")

    def exportCVS(self,expenses):
        import xlsxwriter

        workbook = xlsxwriter.Workbook('RC_a1.xlsx')
        worksheet = workbook.add_worksheet()
        row = 0
        col = 0
        for f,p,a,ar in (expenses):
            worksheet.write(row, col,     f)
            worksheet.write(row, col + 1, p)
            worksheet.write(row, col + 2, a)
            worksheet.write(row, col + 3, ar)
            row += 1
        workbook.close()
        print("funcion para exportar CVS")

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.botonconfig.setText(_translate("MainWindow", "Setting "))
        self.label_4.setText(_translate("MainWindow", "Impedance Measurement"))
        self.label_2.setText(_translate("MainWindow", "Frequency end"))
        self.label.setText(_translate("MainWindow", "Frequency init"))
        self.label_3.setText(_translate("MainWindow", "Voltaje"))
        self.botonStart.setText(_translate("MainWindow", "Start Mesure"))
        self.menuArchivo.setTitle(_translate("MainWindow", "File"))
        self.actionGuardar.setText(_translate("MainWindow", "Exportar CSV"))
        self.actionExportar_txt.setText(_translate("MainWindow", "Exportar TXT"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

