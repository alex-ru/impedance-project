
import readScope 
import lockin
#  
import serial
from PyQt5.QtCore import QThread


import numpy as np
import time
import matplotlib.pyplot as plt
import datetime
#  traza


class Arduino():
    def __init__(self,serialport=None):
        self.serialport = serialport

    def setPort(self,serialport):
        self.serialport = serialport

    def remove(self):
        self.serialport = None

class Data():
    def __init__(self,signal = None,ref=None,fs=None,frequency=100,voltaje=1,pasos=1,frequencyinit=0,frequencyend=200,df=1000):
        self.signal = signal
        self.ref = ref
        self.frequency = frequency
        self.voltaje = voltaje
        self.pasos = pasos 
        self.fs = fs
        self.df = df
        self.__frequencyinit = frequencyinit
        self.__frequencyend = frequencyend

    def getAmpPhase(self):
        [amp,ampr,phase] = lockin.lockin(self.signal,self.ref,self.fs,self.frequency)
        return [amp,ampr,phase]

    def getAmp(self):
        amp1 = lockin.mesureAmp(self.signal)
        amp2 = lockin.mesureAmp(self.ref)
        return amp2
    def clean(self):
        self.signal = []
        self.ref = []
        

class Impedancedata():
    def __init__(self,frequency=[],ampl=[],amplr=[],phase=[],xr=[],xi=[],z=[]):
        self.frequency = frequency
        self.ampl = ampl
        self.amplr = amplr 
        self.phase = phase
        self.xr = xr
        self.xi = xi
        self.z = z
        self.pasos = []

    def addData(self,f,A,Ar,p,paso=None):
        self.frequency.extend([f])
        self.ampl.extend([A])
        self.amplr.extend([Ar])
        self.phase.extend([p])
        self.pasos.extend([paso])

    def addFrequency(self,f):
        self.frequency.extend([f])

    def addAmpl(self,A):
        self.ampl.extend([A])

    def addPhase(self,p):
        self.phase.extend([p])
    
    def dataExp(self):
        f = self.frequency
        p = self.phase
        a  = self.ampl
        ar = self.amplr
        l = list()
        for i in range(len(p)):
            l.append((f[i],p[i],a[i],ar[i]))
        return l

    def dataExpPasos(self):
        f = self.frequency
        p = self.pasos
        a  = self.ampl
        ar = self.amplr
        l = list()
        for i in range(len(p)):
            l.append((f[i],p[i],a[i],ar[i]))
        return l

    def clean(self):
        self.frequency = []
        self.ampl = []
        self.amplr = []
        self.phase = []
        self.pasos = []
        
    def processar(self):
        import math
        self.xr = []
        self.xi = []
        self.z = [] 
        Iv = self.ampl
        fase = self.phase
        Vr  = self.amplr
        for i in range(len(Vr)):
            z = Vr[i]/(Iv[i]/3000) # G =1000000
            self.z.extend([z])
            self.xr.extend([z*math.cos(-math.radians(fase[i]))+0 ]) # calibracion
            self.xi.extend([-z*math.sin(-math.radians(fase[i]))])

data1 = Data()
impedance = Impedancedata()
arduino1 = Arduino()

def getPasos(volts):
    #Volt = 0.0333*paso + 0.1374 relacion por minimos cuadrados
    pasos = int((volts - 0.1374)/0.0333)
    if(pasos<1):
        pasos = 1    #
    if(pasos>70):
        pasos = 70
    return pasos

def setVoltajeArduino(voltaje=None):
    data1.pasos = getPasos(voltaje)
    print("pasos es :"+ str(data1.pasos))
    #arduino = serial.Serial('COM9',baudrate=9600,timeout=0)
    time.sleep(2) #  como eliminarlo 
    dato =  str(data1.pasos)+"d"
    #print(dato)
    bandera = 0
    arduino1.serialport.write(dato.encode())
    dato=''
    response= ''
    while(bandera<100):
        if(arduino1.serialport.in_waiting > 0):
            response = arduino1.serialport.readline()
            #print(response)
        if(('y' in str(response))== True):
            bandera = 101
            #print("Voltaje ajustado" + str(voltaje))  
        bandera = bandera +1
    if(bandera==100):
        u=0
        #print("envio a arduino fallo")
    ##arduino.close()
    ##del arduino
    

def setFrequencyArduino(frequency,pasosArd):
    #arduino = serial.Serial('COM9',baudrate=9600,timeout=0)
    time.sleep(0.5)
    datos5 = str(pasosArd) +"g" + str(frequency)+"z"
    arduino1.serialport.write(datos5.encode()) # se envia la frecuencia con la que se relizara la medicion 
    #arduino.write(b'z')
    #arduino.close()
    #del arduino
    print("frecuencia asignada " + str(frequency))

def finishedMesureArduino():
    #arduino = serial.Serial('COM9',baudrate=9600,timeout=0)
    #time.sleep(2)
    arduino1.serialport.write(b'E')
    arduino1.serialport.close()
    arduino1.remove()
    #del arduino

def plusFrequency(mode='linal',f=0):
    #df = 100
    f = f + data1.df
    return f

def plusPasos(mode='linal',p1=0):
    dpaso = 1
    p1 = p1 + dpaso
    return p1

def testConnectArduino():
    try:
        arduino = serial.Serial("COM9", 9600, timeout=0)
        while arduino.read():
            print('serial open')
        print('serial closed')
        arduino.close()
        return True
    except:
        print('Equipo desconectado')
        return False
    
def control():
    #print("se ejecuta el control ")
    frecuenciaMuestra = data1.frequency
    pasosMuentra  = data1.pasos
    setFrequencyArduino(frecuenciaMuestra,pasosMuentra)
    error = 1
    cont1 = 0
    cont2 = 0
    while((error>0.06)or(error<-0.06)):
        [ref,signal,fs] = readScope.getDataplus(frequency=frecuenciaMuestra)     
        
        #data1.signal = signal2
        #data1.ref = signal1 
        #data1.fs = fs      

        voltReal =   lockin.mesureAmp(ref)
        #voltReal = data1.getAmp()
        
        voltSP =   data1.voltaje

        error = voltReal - voltSP
        print("error : " + str(error))
        print("pasos : "+str(data1.pasos))
        #if((error>0.0333)or(error<-0.0333)):#0.06
        #if((error>0.06)or(error<-0.06)):#0.06
        dpasos = 1
        #dpasos = int((error)/0.0333)
        if(error>0):
            data1.pasos = data1.pasos - dpasos
        if(error<0):
            data1.pasos = data1.pasos + dpasos

        if (data1.pasos<1):
            data1.pasos = 1
            cont1 = cont1 + 1
        if(data1.pasos>90):
            data1.pasos = 90   
            cont2 = cont2 + 1
        pasosMuentra  = data1.pasos 
        setFrequencyArduino(frecuenciaMuestra,pasosMuentra)
        if ((cont1 ==3)or(cont2==3)):
            pasosMuentra  = data1.pasos 
            setFrequencyArduino(frecuenciaMuestra,pasosMuentra)
            return 0

    return 0 


def ReadingHantek():
        #control() 
        pasosMuentra  = data1.pasos
        
        #setFrequencyArduino(frecuenciaMuestra,pasosMuentra)
        #impedance.addData(f=frecuenciaMuestra,A=amp,Ar=ampr,p=phase)

        ## se ejecuta la medicion 
        frecuenciaMuestra =  data1.frequency
        
        setFrequencyArduino(frecuenciaMuestra,pasosMuentra) # cambiar por control
        
        [signal1,signal2,fs] = readScope.getDataplus(frequency=frecuenciaMuestra)     
        data1.signal = signal2
        data1.ref = signal1 
        data1.fs = fs      
        [amp,ampr,phase]=data1.getAmpPhase()
        impedance.addData(f=frecuenciaMuestra,A=amp,Ar=ampr,p=phase,paso=pasosMuentra)
        #print("error 2 : " + str(amp-ampr))
        #print("pasos " + str(pasosMuentra))
        print("Datos leidos y procesados : "+ str(frecuenciaMuestra)+' Hz' + " pasos " + str(pasosMuentra))

class Mesure():
    def __init__(self,filename="cafe_0.txt",fi=200000,ff=10000,df=-1000):
        self.filename = filename
        self.i = 0
        self.config()


        data1.clean()
        impedance.clean()
        data1.pasos = 0
        #plt.ion()    
        #self.fig, self.axs = plt.subplots(2,figsize=(10, 5)) #, constrained_layout=True)
        #self.axs[0].grid()
        #self.axs[1].grid()
        #plt.title("Medicion de impedancia ")
        #plt.show()
        mesured = False
        #fi=200000
        #ff=10000
        #df=-10000 
        data1.df=df
        data1._Data__frequencyinit = fi
        data1._Data__frequencyend  = ff
        data1.frequency  = fi

        while(mesured == False):
            ReadingHantek()
            impedance.processar()
            #self.drawtraza()
            #del self.reading
            if(data1.pasos>80):
                finishedMesureArduino()
                print("Barrido de pasos terminado")
                self.exportTxt(self.filename)
                mesured = True
                #self.exportCVS(impedance.dataExp())
                #return
            if(data1.pasos<=80):
                data1.pasos = plusPasos(mode='lineal',p1=data1.pasos)
                #self.startMesure()


            #axs[0].plot(1,i,'*')  
            #self.i = i
            #self.start()
            #self.drawtraza()
            #self.start()
            #plt.draw()
            #plt.pause(.001)
            #time.sleep(1)
            #plt.pause(0.1)
            #mesured = True
            

        #plt.show() # para hacer stop screem
        #plt.ioff() #Interactive mode off
        


    def drawtraza(self):
        self.axs[0].clear() # funciona 
        self.axs[1].clear() # funciona 

        self.axs[0].plot(impedance.frequency,impedance.z,'*k')
        self.axs[1].plot(impedance.xr,impedance.xi,'*k')
        #i = self.i
        #self.axs[0].plot(i,i,'*r')
        self.axs[0].set_xlabel('frecuencia')
        self.axs[0].set_ylabel('impedance |Z| ')

        self.axs[1].set_xlabel('Zr')
        self.axs[1].set_ylabel('-Zi')
        return 0
        #self.traza_ax.figure.canvas.draw()
    

    def config(self):
        data1.voltaje = 1

        if(testConnectArduino()==True):
            print("conectado")
            arduino1.setPort(serialport=serial.Serial('COM9',baudrate=9600,timeout=0))
            setVoltajeArduino(data1.voltaje)
 

    def start(self,fi=200000,ff=10000,df=-1000):
        
        data1.df=df
        data1._Data__frequencyinit = fi
        data1._Data__frequencyend  = ff
        data1.frequency  = fi
        self.startMesure()

    def readFinished(self):
        print("finish ")
        impedance.processar()
        self.drawtraza()
        #del self.reading
        if(data1.pasos>80):
            finishedMesureArduino()
            print("Barrido de paso terminado")
            self.exportTxt()
            #self.exportCVS(impedance.dataExp())
            return
        if(data1.pasos<=80):
            data1.frequency = plusFrequency(mode='lineal',f=data1.frequency)
            self.startMesure()

    def startMesure(self):
        ReadingHantek()
        self.readFinished()
        #self.drawtraza()

        #self.reading = ReadingHantek1() # 
        #self.reading.finished.connect(self.readFinished) # lo que pasa cuando termina de leer
        #self.reading.start()


    def exportTxt(self,filename):
        ruta = "D:/alejandro\ProyectofGradute/mainProyect/impedance-project/pruevas/calibracion/"
        #filename = "cafe_{}.txt".format(n)
        fileSave = open (ruta+filename,'w')
        m = impedance.dataExpPasos()
        fileSave.write(" f(hz)      Fase(°)     Vs(v)      Vr(v)")    
        fileSave.write('\n')
        for i in range(len(m)):
            for j in range(len(m[0])):
                dato = "{0:.11f}".format(m[i][j])
                if (m[i][j]>0):
                    fileSave.write(dato[0:8])
                else:
                    fileSave.write(dato[0:8])
                fileSave.write("   ")
            fileSave.write('\n')
        fileSave.close()

        print("funcion para exportar txt")

    def exportCVS(self,expenses):
        import xlsxwriter

        workbook = xlsxwriter.Workbook('RC_a1.xlsx')
        worksheet = workbook.add_worksheet()
        row = 0
        col = 0
        for f,p,a,ar in (expenses):
            worksheet.write(row, col,     f)
            worksheet.write(row, col + 1, p)
            worksheet.write(row, col + 2, a)
            worksheet.write(row, col + 3, ar)
            row += 1
        workbook.close()
        print("funcion para exportar CVS")

 

if __name__ == "__main__":
    for i in range(10000,200000,20000):
        #print(i)
        #t = datetime.datetime.now()
        #h = t.hour
        #m = t.minute
        #print(t)
        objeto = Mesure("test_pasos_{}.txt".format(i),i,10000,-10000)
        del objeto
        time.sleep(2)
        

