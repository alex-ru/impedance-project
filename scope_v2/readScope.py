
from PyHT6022.HTSDKScope import Oscilloscope
import matplotlib.pyplot as plt
import numpy as np

# C:/Users/asus/AppData/Local/Programs/Python/Python37-32/python.exe d:/alejandro/ProyectofGradute/ProgramasPYQT/plot/ADCHantek/prueva_v3/scope_v2/readScope.py

class Datascope():
    def __init__(self,signalch1,signalch2,fs):
        self.signalch1 = signalch1
        self.signalch2 = signalch2
        self.fs = fs

def getindex_fs_v(frequency,volt):
    volt_indicies = {
        "20 mV/Div": 0,
        "50 mV/Div": 1, 
        "100 mV/Div":2,
        "200 mV/Div":3,
        "500 mV/Div":4, 
        "1 V/Div": 5,
        "2 V/Div": 6,
        "5 V/Div": 7
    }
    sample_rate_indicies = {
        "48 MSa/s":10,
        "16 MSa/s":11,
        "8 MSa/s":12,
        "4 MSa/s":13,
        "1 MSa/s":14,
        "500 KSa/s":25,
        "200 KSa/s":26,
        "100 KSa/s":27
    }
    indexvol = volt_indicies[volt]
    indexfs  = sample_rate_indicies[frequency]
    
    return[indexfs,indexvol]


def readScope(index_fs=17,scaleVolt_index=5,ndata=10000):# 1MHz y 1 V  por defecto
    #print(index_fs)
    #print(scaleVolt_index)
    #print(ndata)
    scope0 = Oscilloscope(scopeid=0)
    if not scope0.is_attached():
        print("WARNING: Scope not found!")
        exit()
    #scope0.set_voltage_division(2, scaleVolt_index)
    scope0.set_voltage_division(1, scaleVolt_index)
    scope0.set_sampling_rate(index_fs)
    scope0.setup_dso_cal_level()
    data = scope0.read_data_from_scope(data_points=ndata,display_points=ndata)

    #values = [778.0, 311.0, 156.0, 157.0, 127.0, 64.0, 32.0, 13.0] se relacionan con 2 para originar k
    k = [0.002570694087403599, 0.006430868167202572, 0.01282051282051282, 0.012738853503184714, 0.015748031496062992, 0.03125, 0.0625, 0.15384615384615385]
    k1 = k[scaleVolt_index]
    
    signal_ch1 = k1*np.array(data[0])
    signal_ch2 = k1*np.array(data[1])

    return[signal_ch1,signal_ch2]

def getDataplus(frequency=1000,volt=None,ndata=10000):

    if((frequency>=100)and(frequency<500)):
        index_all= getindex_fs_v(frequency="100 KSa/s",volt="1 V/Div")
        fs = 100e3
        ndata = 100000
        
    if((frequency>=500)and(frequency<=1e3)):
        index_all= getindex_fs_v(frequency="100 KSa/s",volt="1 V/Div")
        fs = 100e3
        ndata = 100000

    if((frequency>1e3)and(frequency<=50e3)):
        index_all= getindex_fs_v(frequency="1 MSa/s",volt="1 V/Div")
        fs = 1e6
        ndata = 100000
    if((frequency>50e3)and(frequency<=150e3)):
        index_all= getindex_fs_v(frequency="4 MSa/s",volt="1 V/Div")
        fs = 4e6
        ndata = 100000
    if((frequency>150e3)and(frequency<=1e6)):
        index_all= getindex_fs_v(frequency="4 MSa/s",volt="1 V/Div")
        fs = 4e6
        ndata = 100000

    #index_all= getindex_fs_v(frequency="1 MSa/s",volt="1 V/Div")
    signals = readScope(index_fs=index_all[0],scaleVolt_index=index_all[1],ndata=ndata)
    #print(len(signals[0]))
    data = [signals[0],signals[1],fs]
    #plt.plot(signals[0],'b')
    #plt.plot(signals[1],'k')
    #plt.show()
    return data
    
def test():
    import lockin
    f = 96000
    [vr,vs,fs]=getDataplus(frequency=f,ndata=100000)
    #a = lockin.lockin(vs,vr,fs,f)
    a1 = lockin.mesureAmp(vr)
    a2 = lockin.mesureAmp(vs)
    print(a1)
    print(a2)


#test()
