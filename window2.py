# -*- coding: utf-8 -*-

import scope_v2.readScope 
#  
import serial
from PyQt5.QtCore import QThread

#  traza
from matplotlib.backends.qt_compat import QtCore, QtWidgets, is_pyqt5
if is_pyqt5():
    from matplotlib.backends.backend_qt5agg import (
        FigureCanvas, NavigationToolbar2QT as NavigationToolbar)
else:
    from matplotlib.backends.backend_qt4agg import (
        FigureCanvas, NavigationToolbar2QT as NavigationToolbar)
from matplotlib.figure import Figure
#  traza

from PyQt5 import QtCore, QtGui, QtWidgets

class Datos():
    def __init__(self,signal = None,ref1=None,ref2=None,frequency=100,frequencyinit=0,frequencyend=200):
        self.signal = signal
        self.ref1 = ref1
        self.ref2 = ref2
        self.frequency = frequency
        self.__frequencyinit = frequencyinit
        self.__frequencyend = frequencyend

class Impedancedata():
    def __init__(self,frequency=[],ampl=[],phase=[],xr=[],xi=[],z=[]):
        self.frequency = frequency
        self.ampl = ampl
        self.phase = phase
        self.xr = xr
        self.xi = xi
        self.z = z

    def addFrequency(self,f):
        self.frequency.extend[(f)]

    def addAmpl(self,A):
        self.ampl.extend[(A)]

    def addPhase(self,p):
        self.phase.extend[(p)]
    
    def processar(self):
        self.z =  None
        self.xr = None
        self.xi = None


datos = Datos()

class SendArduino(QThread):
    def __init__(self):
        super().__init__()
           
    def run(self):
        # preconfiguracion del oscilocopio
        arduino = serial.Serial('COM18',baudrate=9600,timeout=0)
        dato1=  "50c2.18d"
        bandera = 0
        arduino.write(dato1.encode())
        d=''
        while(bandera<100):
            if(arduino.in_waiting > 0):
                d = arduino.readline()
                print(d)
            if(('y' in str(d))== True):
                bandera = 101
            bandera = bandera +1
        arduino.close()
        print("Voltaje ajustado")  

class ReadingHantek(QThread):
    def __init__(self):
        super().__init__()
           
    def run(self):
        FrecuenciaMuestra = datos.frequency
        ser = serial.Serial('COM18',baudrate=9600,timeout=0)
        ser.write(str(FrecuenciaMuestra).encode()) # se envia la frecuencia con la que se relizara la medicion 
        ser.write(b'z')
        readScope.getDataplus(frequency=FrecuenciaMuestra)               
        print("Datos leidos y asignados : "+ str(FrecuenciaMuestra)+'Hz')

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setSizeConstraint(QtWidgets.QLayout.SetFixedSize)
        self.gridLayout.setObjectName("gridLayout")
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setObjectName("pushButton")
        self.gridLayout.addWidget(self.pushButton, 5, 2, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem, 1, 2, 1, 1)
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.label_4.setFont(font)
        self.label_4.setAlignment(QtCore.Qt.AlignCenter)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 0, 0, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem1, 7, 2, 1, 1)
        spacerItem2 = QtWidgets.QSpacerItem(58, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem2, 3, 1, 2, 1)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_3.addWidget(self.label_2)
        self.doubleSpinBox_frecuencia_f = QtWidgets.QDoubleSpinBox(self.centralwidget)
        self.doubleSpinBox_frecuencia_f.setMaximum(1000000.0)
        self.doubleSpinBox_frecuencia_f.setObjectName("doubleSpinBox_frecuencia_f")
        self.horizontalLayout_3.addWidget(self.doubleSpinBox_frecuencia_f)
        self.gridLayout.addLayout(self.horizontalLayout_3, 3, 2, 1, 1)
        self.widget = QtWidgets.QWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.widget.sizePolicy().hasHeightForWidth())
        self.widget.setSizePolicy(sizePolicy)
        self.widget.setObjectName("widget")
        self.gridLayout.addWidget(self.widget, 1, 0, 7, 1)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.doubleSpinBox_frecuencia_i = QtWidgets.QDoubleSpinBox(self.centralwidget)
        self.doubleSpinBox_frecuencia_i.setMaximum(1000000.0)
        self.doubleSpinBox_frecuencia_i.setObjectName("doubleSpinBox_frecuencia_i")
        self.horizontalLayout.addWidget(self.doubleSpinBox_frecuencia_i)
        self.gridLayout.addLayout(self.horizontalLayout, 2, 2, 1, 1)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_2.addWidget(self.label_3)
        self.doubleSpinBox_voltaje = QtWidgets.QDoubleSpinBox(self.centralwidget)
        self.doubleSpinBox_voltaje.setDecimals(1)
        self.doubleSpinBox_voltaje.setMaximum(5.0)
        self.doubleSpinBox_voltaje.setObjectName("doubleSpinBox_voltaje")
        self.horizontalLayout_2.addWidget(self.doubleSpinBox_voltaje)
        self.gridLayout.addLayout(self.horizontalLayout_2, 4, 2, 1, 1)
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setObjectName("pushButton_2")
        self.gridLayout.addWidget(self.pushButton_2, 6, 2, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 20))
        self.menubar.setObjectName("menubar")
        self.menuArchivo = QtWidgets.QMenu(self.menubar)
        self.menuArchivo.setObjectName("menuArchivo")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionGuardar = QtWidgets.QAction(MainWindow)
        self.actionGuardar.setObjectName("actionGuardar")
        self.actionExportar_txt = QtWidgets.QAction(MainWindow)
        self.actionExportar_txt.setObjectName("actionExportar_txt")
        self.menuArchivo.addAction(self.actionGuardar)
        self.menuArchivo.addAction(self.actionExportar_txt)
        self.menubar.addAction(self.menuArchivo.menuAction())
        
        self.gridLayouttraza = QtWidgets.QGridLayout(self.widget)
        traza = FigureCanvas(Figure(figsize=(5, 3)))
        self.gridLayouttraza.addWidget(traza)
        self.traza_ax = traza.figure.subplots()

        self.actionExportar_txt.triggered.connect(self.exportTxt)
        self.actionGuardar.triggered.connect(self.exportCVS)

        self.pushButton.clicked.connect(self.drawtraza)
        
        self.t= -20 # variable dinamica para probar grafica
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
    
    def drawtraza(self):
        import numpy as np
        #self.traza_ax.clear()
        self.t = self.t + 1 
        t = self.t
        y = 20 - t*t
        print(datos._Datos__frequencyinit)
        self.traza_ax.plot(t,y,'*r')
        self.traza_ax.grid()
        self.traza_ax.figure.canvas.draw()

    def exportTxt(self):
        print("funcion para exportar txt")
    def exportCVS(self):
        print("funcion para exportar CVS")
    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.pushButton.setText(_translate("MainWindow", "Configurar Equipo"))
        self.label_4.setText(_translate("MainWindow", "Impedance Measurement"))
        self.label_2.setText(_translate("MainWindow", "Frecuencia final"))
        self.label.setText(_translate("MainWindow", "Frecuencia inicial"))
        self.label_3.setText(_translate("MainWindow", "Voltaje"))
        self.pushButton_2.setText(_translate("MainWindow", "Iniciar medicion"))
        self.menuArchivo.setTitle(_translate("MainWindow", "archivo"))
        self.actionGuardar.setText(_translate("MainWindow", "Exportar CSV"))
        self.actionExportar_txt.setText(_translate("MainWindow", "Exportar TXT"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

